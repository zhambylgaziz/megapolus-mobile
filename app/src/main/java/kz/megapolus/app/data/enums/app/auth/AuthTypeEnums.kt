package kz.megapolus.app.data.enums.app.auth

enum class AuthTypeEnums(val id: String) {

    PASSWORD("password"),
    PIN("pin"),
    BIOMETRIC("biometric")

}