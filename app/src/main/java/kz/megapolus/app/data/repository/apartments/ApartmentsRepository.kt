package kz.megapolus.app.data.repository.apartments

import androidx.lifecycle.LiveData
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.apartment.BookingStatus
import kz.megapolus.app.data.models.apartment.Review
import kz.megapolus.app.data.models.network.Resource

interface ApartmentsRepository {

    fun getApartments(): LiveData<Resource<List<Apartment>>>

    fun getApartment(id: String): LiveData<Resource<Apartment>>

    fun postApartments(params: Map<String, Any>): LiveData<Resource<Apartment>>

    fun editApartment(id: String, params: Map<String, Any>): LiveData<Resource<Apartment>>

    fun getReview(): LiveData<Resource<List<Review>>>

    fun postReview(params: Map<String, Any>): LiveData<Resource<Review>>

    fun getBookings(): LiveData<Resource<List<Booking>>>

    fun postBooking(params: Map<String, Any>): LiveData<Resource<Booking>>

    fun getBookingStatuses(): LiveData<Resource<List<BookingStatus>>>

    fun postBookingStatus(params: Map<String, Any>): LiveData<Resource<BookingStatus>>

    fun deleteApartment(id: String): LiveData<Resource<Unit>>


}