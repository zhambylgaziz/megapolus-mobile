package kz.megapolus.app.data.auth_state

import kz.megapolus.app.data.preferences.Preferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthState
@Inject constructor(
    private val preferences: Preferences
) {

    val isAuthorized: Boolean
        get() = !preferences.getAppToken().isNullOrBlank()

}