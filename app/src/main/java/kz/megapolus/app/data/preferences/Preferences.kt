package kz.megapolus.app.data.preferences

interface Preferences {

    /**
     *
     */
    fun setUniqueID(uniqueID: String?)

    fun getUniqueID(): String?

    fun setLanguage(language: String)

    fun getLanguage(): String?

    /**
     *
     */
    fun setAppToken(appToken: String)

    fun getAppToken(): String?

    fun removeAppToken()

    //
    fun isOnboardShown(): Boolean

    fun setOnboardShown()

    //
    fun setAuthType(authType: String)

    fun getAuthType(): String

    fun setDevicePIN(pin: String?)

    fun getDevicePIN(): String?

    //
    fun setLogin(login: String?)

    fun getLogin(): String?

    //
    fun setSavedList(list: List<String>)

    fun getSavedList(): List<String>

}