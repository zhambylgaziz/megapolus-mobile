package kz.megapolus.app.data.models.auth

data class Token(
    val authToken: String?
)