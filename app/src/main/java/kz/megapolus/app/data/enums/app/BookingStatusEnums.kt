package kz.megapolus.app.data.enums.app

enum class BookingStatusEnums(val id: Int) {

    CREATED(1),
    CONFIRMED(2),
    WAITING_PAYMENT(3),
    PAID(4),
    IN_PROGRESS(5),
    FINISHED(6),
    CANCELLED(7),
    REJECTED(8),

}