package kz.megapolus.app.data.models.house

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Facility(
    val id: String?,
    val name: String?,
    val icon: String?
) : Parcelable