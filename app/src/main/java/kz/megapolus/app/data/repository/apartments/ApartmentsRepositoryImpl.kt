package kz.megapolus.app.data.repository.apartments

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.apartment.BookingStatus
import kz.megapolus.app.data.models.apartment.Review
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.repository.error_handler.RepositoryErrorHandler
import kz.megapolus.app.network.api.ApartmentsApi
import javax.inject.Inject

class ApartmentsRepositoryImpl
@Inject constructor(
    private val api: ApartmentsApi,
    private val errorHandler: RepositoryErrorHandler
) : ApartmentsRepository {

    override fun getApartments(): LiveData<Resource<List<Apartment>>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getApartments()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<List<Apartment>>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<List<Apartment>>(e)
                )
            }
        }
    }

    override fun getApartment(id: String): LiveData<Resource<Apartment>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getApartment(id)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Apartment>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Apartment>(e)
                )
            }
        }
    }

    override fun postApartments(params: Map<String, Any>): LiveData<Resource<Apartment>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.postApartment(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Apartment>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Apartment>(e)
                )
            }
        }
    }

    override fun editApartment(
        id: String,
        params: Map<String, Any>
    ): LiveData<Resource<Apartment>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.patchApartment(id, params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Apartment>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Apartment>(e)
                )
            }
        }
    }

    override fun deleteApartment(id: String): LiveData<Resource<Unit>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.deleteApartment(id)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Unit>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Unit>(e)
                )
            }
        }
    }

    override fun getReview(): LiveData<Resource<List<Review>>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getReview()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<List<Review>>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<List<Review>>(e)
                )
            }
        }
    }

    override fun postReview(params: Map<String, Any>): LiveData<Resource<Review>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.postReview(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Review>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Review>(e)
                )
            }
        }
    }

    override fun getBookings(): LiveData<Resource<List<Booking>>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getBooking()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<List<Booking>>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<List<Booking>>(e)
                )
            }
        }
    }

    override fun postBooking(params: Map<String, Any>): LiveData<Resource<Booking>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.postBooking(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Booking>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Booking>(e)
                )
            }
        }
    }

    override fun getBookingStatuses(): LiveData<Resource<List<BookingStatus>>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getBookingStatuses()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<List<BookingStatus>>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<List<BookingStatus>>(e)
                )
            }
        }
    }

    override fun postBookingStatus(params: Map<String, Any>): LiveData<Resource<BookingStatus>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.postBookingStatus(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<BookingStatus>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<BookingStatus>(e)
                )
            }
        }
    }
}