package kz.megapolus.app.data.enums.app

enum class OtpTypeEnums(val id: String) {

    //login
    SET_FROM_AUTH("set_from_auth"),
    CHECK_TO_AUTH("check_to_auth"),
    SET_FROM_REGISTRATION("set_from_registration"),

    //settings
    SET_FROM_SETTINGS("set_from_settings"),
    CHANGE("change"),
    CHECK_TO_DISABLE("check_to_disable"),
    CHECK_TO_CHANGE("check_to_change")

}