package kz.megapolus.app.data.repository.about

import androidx.lifecycle.LiveData
import kz.megapolus.app.data.models.network.Resource

interface AboutRepository {

    fun getActions(params: Map<String, String>): LiveData<Resource<Any>>

//    fun getAction(id: String): LiveData<Resource<ActionResponse>>

}