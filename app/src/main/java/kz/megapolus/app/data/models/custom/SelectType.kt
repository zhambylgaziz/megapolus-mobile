package kz.megapolus.app.data.models.custom

import kz.megapolus.app.data.enums.app.RequestCodeEnums

data class SelectType(
    val code: RequestCodeEnums,
    val list: List<Any>
)