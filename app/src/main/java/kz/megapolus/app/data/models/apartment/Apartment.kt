package kz.megapolus.app.data.models.apartment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kz.megapolus.app.data.enums.app.DatePatternEnums
import kz.megapolus.app.utils.convertDateToString
import kz.megapolus.app.utils.convertStringToDate

@Parcelize
data class Apartment(
    val id: String?,
    val user: String?,
    val createdAt: String?,
    var district: String?,
    var street: String?,
    var house: String?,
    var apartment: String?,
    var price: String?,
    var description: String?,
    var term: String?,
    @SerializedName("numberOfRooms") var rooms: String?,
    var area: String?,
    @SerializedName("img_link") var imageLink: String?,
    @SerializedName("numOfFloor") var flat: String?,
    @SerializedName("numberOfFloorsOfTheHouse") var numberOfFloorsOfTheHouse: String?,
    @SerializedName("yearOfConstruction") var yearOfConstruction: String?,
    var enabled: Boolean?,
    var bookingEnabled: Boolean?,
    var lat: Long?,
    var lng: Long?,
    var reviews: List<Review>,
    var review: String? = null
) : Parcelable {
    fun getAddress(): String {
        var address = "$district р., улица $street, дом $house"
        if (apartment != null) {
            address += ", кв $apartment"
        }
        return address
    }

    fun getRoomsNumber(): String {
        return "Количество комнат: $rooms"
    }

    fun getApartmentsArea(): String {
        return "$area м²"
    }

    fun getCreatedDate(): String {
        return if (createdAt != null) {
            val date = convertStringToDate(createdAt, DatePatternEnums.FULL.id)
            if (date != null)
                convertDateToString(date, DatePatternEnums.DDMMYYYY_BY_PERIOD.id)
            else {
                ""
            }
        } else {
            ""
        }
    }

}