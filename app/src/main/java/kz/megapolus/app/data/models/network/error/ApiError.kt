package kz.megapolus.app.data.models.network.error

data class ApiError(
    val message: String?
)