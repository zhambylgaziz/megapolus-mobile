package kz.megapolus.app.data.repository.auth

import androidx.lifecycle.LiveData
import kz.megapolus.app.data.models.auth.Token
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.utils.live_data.Event

interface AuthRepository {

    fun authLogin(params: Map<String, String>): LiveData<Resource<Token>>

    fun authRegister(params: Map<String, String>): LiveData<Resource<User>>

    fun getUser(): LiveData<Resource<User>>

    fun logout(): LiveData<Event<Resource<Unit>>>

}