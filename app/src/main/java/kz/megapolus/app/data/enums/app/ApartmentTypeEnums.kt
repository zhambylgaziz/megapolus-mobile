package kz.megapolus.app.data.enums.app

enum class ApartmentTypeEnums(val id: String) {

    HOURLY("1"),
    DAILY("2"),
    MONTHLY("3"),
    QUARTERLY("4")

}