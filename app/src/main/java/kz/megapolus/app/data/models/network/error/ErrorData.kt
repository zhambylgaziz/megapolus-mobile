package kz.megapolus.app.data.models.network.error

data class ErrorData(
    val fields: Map<String, String>?
)