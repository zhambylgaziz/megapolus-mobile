package kz.megapolus.app.data.preferences

import android.content.Context
import android.content.SharedPreferences
import kz.megapolus.app.data.enums.app.auth.AuthTypeEnums

class PreferencesImpl(
    private val context: Context
) : Preferences {

    companion object {
        private const val UNIQUE_ID = "unique_id"
        private const val LANGUAGE = "language"
        private const val APP_TOKEN = "app_token"
        private const val ONBOARD_SHOWN = "onboard_shown"
        private const val DEVICE_PIN = "device_pin"
        private const val AUTH_TYPE = "auth_type"
        private const val LOGIN = "login"
        private const val SAVED = "saved"
    }


    private lateinit var shPr: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    init {
        shPr = context.getSharedPreferences("PreferencesImpl", Context.MODE_PRIVATE)
        editor = shPr.edit()
    }

    /**
     * DEVICE UNIQUE ID
     */
    override fun setUniqueID(uniqueID: String?) {
        editor.putString(UNIQUE_ID, uniqueID)
        editor.commit()
    }

    override fun getUniqueID(): String? = shPr.getString(UNIQUE_ID, null)

    /**
     * LANGUAGE
     */
    override fun setLanguage(language: String) {
        editor.putString(LANGUAGE, language)
        editor.commit()
    }

    override fun getLanguage(): String? {
        return shPr.getString(LANGUAGE, null)
    }

    /**
     *
     */
    override fun setAppToken(appToken: String) {
        editor.putString(APP_TOKEN, appToken)
        editor.commit()
    }

    override fun getAppToken(): String? {
        return shPr.getString(APP_TOKEN, null)
    }

    override fun removeAppToken() {
        editor
            .remove(APP_TOKEN)
            .commit()
    }

    //
    override fun isOnboardShown(): Boolean {
        return shPr.getBoolean(ONBOARD_SHOWN, false)
    }

    override fun setOnboardShown() {
        editor.putBoolean(ONBOARD_SHOWN, true)
        editor.commit()
    }

    //
    override fun setAuthType(authType: String) {
        editor.putString(AUTH_TYPE, authType)
        editor.commit()
    }

    override fun getAuthType(): String {
        return shPr.getString(AUTH_TYPE, AuthTypeEnums.PASSWORD.id) ?: AuthTypeEnums.PASSWORD.id
    }

    override fun setDevicePIN(pin: String?) {
        editor.putString(DEVICE_PIN, pin)
        editor.commit()
    }

    override fun getDevicePIN(): String? {
        return shPr.getString(DEVICE_PIN, "")
    }

    //
    override fun setLogin(login: String?) {
        editor.putString(LOGIN, login)
        editor.commit()
    }

    override fun getLogin(): String? {
        return shPr.getString(LOGIN, "")
    }

    //
    override fun setSavedList(list: List<String>) {
        editor.putStringSet(SAVED, list.toSet())
        editor.commit()
    }

    override fun getSavedList(): List<String> {
        val set = shPr.getStringSet(SAVED, emptySet())
        return set?.toList() ?: emptyList()
    }
}