package kz.megapolus.app.data.models.apartment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kz.megapolus.app.data.enums.app.DatePatternEnums
import kz.megapolus.app.utils.convertDateToString
import kz.megapolus.app.utils.convertStringToDate

@Parcelize
data class Review(
    val id: String?,
    val created: String?,         // "2020-05-04T22:06:42.763874Z",
    val star: Double?,         // 5.0,
    val review: String?,         // "Хороший владелец",
    val name: String?,         // "Ален",
    val user: String?,         // "https://megapolus.herokuapp.com/auth/v1/users/1/",
    val apartment: String?         // "https://megapolus.herokuapp.com/apartments/3/"

) : Parcelable {
    fun getCreatedDate(): String {
        return if (created != null) {
            val date = convertStringToDate(created, DatePatternEnums.FULL.id)
            if (date != null)
                convertDateToString(date, DatePatternEnums.DDMMYYYY_BY_PERIOD.id)
            else {
                ""
            }
        } else {
            ""
        }
    }
}