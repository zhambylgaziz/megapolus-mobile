package kz.megapolus.app.data.models.apartment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kz.megapolus.app.data.enums.app.DatePatternEnums
import kz.megapolus.app.utils.convertDateToString
import kz.megapolus.app.utils.convertStringToDate

@Parcelize
data class Booking(
    val id: String?,
    val created: String?,
    val dateStart: String?,
    val dateEnd: String?,
    val description: String?,
    val user: String?,
    val apartment: String?,
    val apartmentOwner: String?,
    var owner: Boolean = false,
    val statuses: List<BookingStatus>?,
    var statusText: String? = "Отправлен"
) : Parcelable {
    fun getCreatedDate(): String {
        return if (created != null) {
            val date = convertStringToDate(created, DatePatternEnums.FULL.id)
            if (date != null)
                convertDateToString(date, DatePatternEnums.DDMMYYYY_BY_PERIOD.id)
            else {
                ""
            }
        } else {
            ""
        }
    }
}