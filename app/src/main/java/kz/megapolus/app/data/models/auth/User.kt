package kz.megapolus.app.data.models.auth

data class User(
    val id: String?,
    val username: String?,
    val email: String?
)