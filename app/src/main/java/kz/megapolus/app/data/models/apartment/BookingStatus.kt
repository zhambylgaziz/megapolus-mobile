package kz.megapolus.app.data.models.apartment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kz.megapolus.app.data.enums.app.DatePatternEnums
import kz.megapolus.app.utils.convertDateToString
import kz.megapolus.app.utils.convertStringToDate

@Parcelize
data class BookingStatus(
    val id: String?,
    val url: String?,
    val created: String?,
    val term: Int?,
    val booking: String?
) : Parcelable {
    fun getCreatedDate(): String {
        return if (created != null) {
            val date = convertStringToDate(created, DatePatternEnums.FULL.id)
            if (date != null)
                convertDateToString(date, DatePatternEnums.DDMMYYYY_BY_PERIOD.id)
            else {
                ""
            }
        } else {
            ""
        }
    }
}