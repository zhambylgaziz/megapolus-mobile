package kz.megapolus.app.data.repository.about

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.error_handler.RepositoryErrorHandler
import kz.megapolus.app.network.api.AboutApi
import javax.inject.Inject

class AboutRepositoryImpl
@Inject constructor(
    private val api: AboutApi,
    private val errorHandler: RepositoryErrorHandler,
    private val preferences: Preferences
) : AboutRepository {

    override fun getActions(params: Map<String, String>): LiveData<Resource<Any>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getActions(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Any>(httpStatusCode = code(), errorBody = errorBody())
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Any>(e)
                )
            }
        }
    }
//
//    override fun getAction(id: String): LiveData<Resource<ActionResponse>> {
//        return liveData {
//            try {
//                emit(
//                    Resource.loading(null)
//                )
//
//                val request = api.getAction(id)
//                with(request) {
//                    if (isSuccessful) {
//                        emit(
//                            Resource.success(body())
//                        )
//                    } else {
//                        emit(
//                            errorHandler.getError(code(), errorBody())
//                        )
//                    }
//                }
//            } catch (e: Exception) {
//                emit(
//                    errorHandler.getError<ActionResponse>(e)
//                )
//            }
//        }
//    }

}