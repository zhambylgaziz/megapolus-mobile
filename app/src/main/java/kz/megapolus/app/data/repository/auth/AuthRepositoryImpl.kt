package kz.megapolus.app.data.repository.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kz.megapolus.app.data.models.auth.Token
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.repository.error_handler.RepositoryErrorHandler
import kz.megapolus.app.network.api.AuthApi
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class AuthRepositoryImpl
@Inject constructor(
    private val api: AuthApi,
    private val errorHandler: RepositoryErrorHandler
) : AuthRepository {

    override fun authLogin(params: Map<String, String>): LiveData<Resource<Token>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.authLogin(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<Token>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<Token>(e)
                )
            }
        }
    }

    override fun authRegister(params: Map<String, String>): LiveData<Resource<User>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.authRegister(params)
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<User>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<User>(e)
                )
            }
        }
    }

    override fun getUser(): LiveData<Resource<User>> {
        return liveData {
            try {
                emit(
                    Resource.loading(null)
                )

                val request = api.getUser()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Resource.success(body())
                        )
                    } else {
                        emit(
                            errorHandler.getError<User>(
                                httpStatusCode = code(),
                                errorBody = errorBody()
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    errorHandler.getError<User>(e)
                )
            }
        }
    }

    override fun logout(): LiveData<Event<Resource<Unit>>> {
        return liveData {
            try {
                emit(
                    Event(Resource.loading(null))
                )

                val request = api.logout()
                with(request) {
                    if (isSuccessful) {
                        emit(
                            Event(Resource.success(body()))
                        )
                    } else {
                        emit(
                            Event(
                                errorHandler.getError<Unit>(
                                    httpStatusCode = code(),
                                    errorBody = errorBody()
                                )
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                emit(
                    Event(errorHandler.getError<Unit>(e))
                )
            }
        }
    }
}