package kz.megapolus.app.data.models.house

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class House(
    val id: String? = null,
    val image: Int? = null,
    val imageBig: Int? = null,
    val imageSmall: Int? = null,
    val price: String? = null,
    val rooms: String? = null,
    val address: String? = null,
    val lat: String? = null,
    val lng: String? = null,
    val beds: String? = null,
    val baths: String? = null,
    val area: String? = null,
    val facilities: List<Facility>? = null,
    var saved: Boolean = false,
    val description: String? = null
): Parcelable