package kz.megapolus.app.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kz.megapolus.app.R
import kz.megapolus.app.databinding.FragmentSplashBinding
import kz.megapolus.app.ui.activities.main.MainActivity
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideUpAnimBuilder

class SplashFragment : BaseFragment() {

    private lateinit var binding: FragmentSplashBinding
    private lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(SplashViewModel::class.java)
        binding.viewModel = viewModel

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.openLogin.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val intent = MainActivity.getIntent(context)
                    startActivity(intent)

                    activity?.finish()

                }
            }
        )

        viewModel.openOnboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        SplashFragmentDirections.actionSplashFragmentToOnboardViewPagerFragment()

                    val navBuilder = getSlideUpAnimBuilder()
                    navBuilder.setPopUpTo(R.id.splashFragment, true)

                    findNavController().navigate(
                        action,
                        navBuilder.build()
                    )
                }
            }
        )

        viewModel.openCheckPIN.observe(
            viewLifecycleOwner,
            Observer {
//                it.getContentIfNotHandled()?.let {
//                    val pendingIntent = NavDeepLinkBuilder(context!!)
//                        .setComponentName(UnauthorizedActivity::class.java)
//                        .setGraph(R.navigation.activity_unauthorized)
//                        .setDestination(R.id.checkPINFragment)
//                        .setArguments(
//                            CheckPINFragmentArgs(
//                                otpType = OtpTypeEnums.CHECK_TO_AUTH
//                            ).toBundle()
//                        )
//                        .createPendingIntent()
//
//                    val intent = Intent()
//                    pendingIntent.send(context, 0, intent)
//
//                    activity?.finish()
//                }
            }
        )
    }

}