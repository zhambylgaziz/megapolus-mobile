package kz.megapolus.app.ui.registration.password

interface PasswordRequirementsCallback {

    fun passwordOk(password: String): Boolean

}