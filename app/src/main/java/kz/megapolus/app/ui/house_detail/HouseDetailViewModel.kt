package kz.megapolus.app.ui.house_detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Review
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.house.House
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class HouseDetailViewModel
@Inject constructor(
    private val app: Application,
    private val preferences: Preferences,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    lateinit var apartment: Apartment

    val houseSavedChecked = MutableLiveData<Boolean>()
    val bookingButtonVisible = MutableLiveData<Boolean>()

    private var user: User? = null

    fun setArgs(apartment: Apartment, user: User?) {
        this.user = user
        this.apartment = apartment
        val saved = preferences.getSavedList()
        houseSavedChecked.postValue(saved.contains(apartment.id))
        bookingButtonVisible.postValue(user?.id != apartment.user && user != null)

        _refresh.postValue(Unit)
    }

    fun onStarClick() {
        val checked = houseSavedChecked.value == true
        val saved = preferences.getSavedList()
        val temp = mutableListOf<String>()
        temp.addAll(saved)
        if (checked) {
            temp.remove(apartment.id)
        } else {
            apartment.id?.let {
                temp.add(it)
            }
        }
        preferences.setSavedList(temp)
        houseSavedChecked.postValue(!checked)
    }

    /**
     * Network
     */
    private val _refresh = MutableLiveData<Unit>()

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    val reviewResource: LiveData<Resource<List<Review>>> =
        Transformations.switchMap(_refresh) {
            repository.getReview()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(reviewResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(reviewResource) {
            it.status
        }

    private val _list = MutableLiveData<List<Review>>()
    val list: LiveData<List<Review>>
        get() = _list

    fun onSuccess(list: List<Review>?) {
        val temp = mutableListOf<Review>()

        list?.forEach { review ->
            if (review.apartment == apartment.id) {
                temp.add(review)
            }
        }

        _list.postValue(temp)
    }

    val recyclerViewItemClickCallback = object :
        RecyclerViewItemClickCallback {
        override fun onRecyclerViewItemClick(any: Any) {
            if (any is Review) {

            }
        }
    }

    private val _makeBooking = MutableLiveData<Event<Apartment>>()
    val makeBooking: LiveData<Event<Apartment>>
        get() = _makeBooking


    fun onBookBtnClick() {
        _makeBooking.postValue(Event(apartment))
    }

}
