package kz.megapolus.app.ui.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentExploreBinding
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class ExploreFragment : BaseFragment() {

    private lateinit var binding: FragmentExploreBinding
    private lateinit var viewModel: ExploreViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.rvTopRated.layoutManager =
//            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        binding.rvAll.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel(ExploreViewModel::class.java)
        binding.viewModel = viewModel

//        binding.rvTopRated.adapter = TopRatedAdapter(viewModel.recyclerViewItemClickCallback)
        binding.rvAll.adapter = ApartmensAdapter(viewModel.recyclerViewItemClickCallback)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apartmentsResource.observe(
            viewLifecycleOwner,
            Observer {
                when(it.status){
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                }
            }
        )
        viewModel.navigate.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        ExploreFragmentDirections.actionExploreFragmentToNavigationHouseDetail(it)
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.rvAll.adapter as ApartmensAdapter).submitList(it)
            }
        )
    }
}