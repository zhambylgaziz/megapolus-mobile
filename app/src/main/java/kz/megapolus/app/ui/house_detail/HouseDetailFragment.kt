package kz.megapolus.app.ui.house_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Review
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentHouseDetailBinding
import kz.megapolus.app.ui.activities.main.MainViewModel
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class HouseDetailFragment : BaseFragment() {

    private val args: HouseDetailFragmentArgs by navArgs()

    private lateinit var binding: FragmentHouseDetailBinding
    private lateinit var viewModel: HouseDetailViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_house_detail, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(HouseDetailViewModel::class.java)
        viewModel.setArgs(args.apartment, userViewModel.getUser())

        binding.viewModel = viewModel
        binding.apartment = viewModel.apartment
        binding.recyclerView.adapter =
            ReviewAdapter(viewModel.recyclerViewItemClickCallback)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.reviewResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                }
            }
        )
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.recyclerView.adapter as ReviewAdapter).submitList(it)
            }
        )
        viewModel.makeBooking.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        HouseDetailFragmentDirections.actionHouseDetailFragmentToBookingFragment(it)
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )

    }

}