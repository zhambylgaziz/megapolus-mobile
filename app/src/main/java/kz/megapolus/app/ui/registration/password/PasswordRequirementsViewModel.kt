package kz.megapolus.app.ui.registration.password

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class PasswordRequirementsViewModel
@Inject constructor(
    private val app: Application
) : AndroidViewModel(app) {

    val length = MutableLiveData<Boolean>()
    val requireDifferentRegister = MutableLiveData<Boolean>()
    val requireNumbers = MutableLiveData<Boolean>()
    val requireCharacters = MutableLiveData<Boolean>()
    val latinCharacters = MutableLiveData<Boolean>()

    val passwordRequirementsCallback = object :
        PasswordRequirementsCallback {
        override fun passwordOk(password: String): Boolean {
            return verify(password)
        }
    }

    private fun verify(password: String): Boolean {
        val lengthOk = checkLength(password)
        val requireDifferentRegisterOk = checkRequireDifferentRegister(password)
        val requireNumbersOk = checkRequireNumbers(password)
        val requireCharactersOk = checkRequireCharacters(password)

        length.postValue(lengthOk)
        requireDifferentRegister.postValue(requireDifferentRegisterOk)
        requireNumbers.postValue(requireNumbersOk)
        requireCharacters.postValue(requireCharactersOk)

        return lengthOk
                && requireDifferentRegisterOk
                && requireNumbersOk
                && requireCharactersOk
    }

    //^(?=.*[A-Za-z])(?=.*\d)(?=.*[!@#$%^&*?_~+=-])[A-Za-z\d!@#$%^&*?_~+=-]{8,}$
    private fun checkLength(password: String): Boolean {
        return password.length in 8..14
    }

    private fun checkRequireDifferentRegister(password: String): Boolean {
        return password != password.toLowerCase() && password != password.toUpperCase()
    }

    private fun checkRequireNumbers(password: String): Boolean {
        return password.matches(".*\\d.*".toRegex())
    }

    private fun checkRequireCharacters(password: String): Boolean {
        return password.matches((".*[${getRawCharacters()}].*").toRegex())
    }

//    private fun getRawCharacters() =
//        "\\.\\,\\:\\;\\?\\!\\*\\+\\%\\-\\<\\>\\@\\[\\]\\{\\}\\/\\_\\{\\}\\$\\#"
    private fun getRawCharacters() = "\\!\\@\\#\\$\\%\\^\\&"

}