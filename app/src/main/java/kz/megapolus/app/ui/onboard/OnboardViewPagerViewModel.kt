package kz.megapolus.app.ui.onboard

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class OnboardViewPagerViewModel
@Inject constructor(
    private val app: Application,
    private val preferences: Preferences
) : AndroidViewModel(app) {

    /**
     *
     */
    private var position: Int = 0

    val onPageChangeCallback = object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            this@OnboardViewPagerViewModel.position = position
            if (position == 4) {
                nextBtnVisible.postValue(false)
                skipBtnVisible.postValue(false)
                startBtnVisible.postValue(true)
            } else {
                nextBtnVisible.postValue(true)
                skipBtnVisible.postValue(true)
                startBtnVisible.postValue(false)
            }
        }

        override fun onPageScrollStateChanged(state: Int) {
            //do nothing
        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            //do nothing
        }
    }

    /**
     *
     */
    private val _navigateNext = MutableLiveData<Event<Unit>>()
    val navigateNext: LiveData<Event<Unit>>
        get() = _navigateNext

    val skipBtnVisible = MutableLiveData<Boolean>()
    val nextBtnVisible = MutableLiveData<Boolean>()
    val startBtnVisible = MutableLiveData<Boolean>()

    init {
        nextBtnVisible.value = true
        skipBtnVisible.value = true
    }

    fun onNextBtnClick() {
        _setCurrentItem.postValue(Event(position + 1))
    }

    private val _setCurrentItem = MutableLiveData<Event<Int>>()
    val setCurrentItem: LiveData<Event<Int>>
        get() = _setCurrentItem

    fun onSkipBtnClick() {
        preferences.setOnboardShown()
        _navigateNext.postValue(Event(Unit))
    }
}