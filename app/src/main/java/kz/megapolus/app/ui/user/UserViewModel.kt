package kz.megapolus.app.ui.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class UserViewModel
@Inject constructor(
    private val app: Application
) : AndroidViewModel(app) {

    private var user: User? = null

    fun onSuccess(user: User?) {
        this.user = user
    }

    fun getUser() = user

    /**
     * Login State
     */
    fun onUserSuccess() {
        _refreshState.postValue(Event(Unit))
    }

    private val _refreshState = MutableLiveData<Event<Unit>>()
    val refreshState: LiveData<Event<Unit>>
        get() = _refreshState

}