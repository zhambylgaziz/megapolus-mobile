package kz.megapolus.app.ui.registration

import kz.megapolus.app.ui_common.base.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentRegistrationBinding
import kz.megapolus.app.ui.activities.main.MainViewModel
import kz.megapolus.app.ui.registration.password.PasswordRequirementsViewModel
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.utils.KeyboardUtils

class RegistrationFragment : BaseFragment() {

    private lateinit var binding: FragmentRegistrationBinding
    private lateinit var viewModel: RegistrationViewModel
    private lateinit var passwordRequirementsViewModel: PasswordRequirementsViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        userViewModel = getViewModelOfActivity(UserViewModel::class.java)
        passwordRequirementsViewModel = getViewModel(PasswordRequirementsViewModel::class.java)
        viewModel = getViewModel(RegistrationViewModel::class.java)
        viewModel.setPasswordRequirementsCallback(passwordRequirementsViewModel.passwordRequirementsCallback)
        mainViewModel = getViewModelOfActivity(MainViewModel::class.java)

        binding.viewModel = viewModel
        binding.passwordRequirementsViewModel = passwordRequirementsViewModel
        binding.etPassword.onFocusChangeListener = viewModel.listener

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.registerClientResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        showDialog()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.authResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        viewModel.onSuccess(it.data)
                        userViewModel.onUserSuccess()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.updateButton.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    mainViewModel.updateButtonVisibility()
                    findNavController().popBackStack(R.id.profileFragment, false)
                }
            }
        )
        viewModel.hideKeyboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    KeyboardUtils.hideKeyboard(
                        context,
                        binding.btnLogin
                    )
                }
            }
        )
    }

    private fun showDialog() {
        AlertDialog
            .Builder(context!!)
            .setTitle(getString(R.string.registration_success_title))
            .setMessage(getString(R.string.registration_success_text))
            .setPositiveButton(getString(R.string.registration_success_btn_ok)) { dialog, which ->
                viewModel.onRegistrationSuccess()
            }
            .create()
            .show()
    }

}