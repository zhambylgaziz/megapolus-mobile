package kz.megapolus.app.ui.explore

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.house.House
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.repository.about.AboutRepository
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.ui_common.callbacks.SwipeToRefreshCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class ExploreViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    private val _refresh = MutableLiveData<Unit>()

    init {
        _refresh.postValue(Unit)
    }

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     *
     */
    var isRefreshing = MutableLiveData<Boolean>()

    init {
        isRefreshing.value = false
    }

    val swipeToRefreshCallback = object :
        SwipeToRefreshCallback {
        override fun onSwipeToRefresh() {
            _refresh.value = Unit
            isRefreshing.value = false
        }
    }

    /**
     * Network
     */
    val apartmentsResource: LiveData<Resource<List<Apartment>>> =
        Transformations.switchMap(_refresh) {
            repository.getApartments()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(apartmentsResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(apartmentsResource) {
            it.status
        }

    /**
     * Content
     */
    private val _list = MutableLiveData<List<Apartment>>()
    val list: LiveData<List<Apartment>>
        get() = _list

    fun onSuccess(list: List<Apartment>?) {
        val temp = mutableListOf<Apartment>()

        list?.let {
            it.forEach { apartment ->
                if (apartment.enabled == true) {
                    var total = 0.0
                    val size = if (!apartment.reviews.isNullOrEmpty()) {
                        apartment.reviews.size
                    } else 1
                    apartment.reviews.forEach { review ->
                        review.star?.let { star ->
                            total += star
                        }
                    }
                    apartment.review = (total / size).toString()
                    temp.add(apartment)
                }
            }
        }

        _list.postValue(temp)
    }

    /**
     * Header
     */
    val location = MutableLiveData<String>()

    init {
        location.postValue("Almaty")
    }

    fun onSettingsClick() {

    }

    fun onAllBtnClick() {

    }

    /**
     * Search
     */
    val searchFieldText = MutableLiveData<String>()

    fun onSearchFieldTextChanged() {

    }

    /**
     *
     */
    private val _listTopRated = MutableLiveData<List<House>>()
    val listTopRated: LiveData<List<House>>
        get() = _listTopRated

    fun initTopRatedList(list: List<House>) {
        val temp = mutableListOf<House>()

        list.forEach { house ->
            if (house.imageSmall != null) {
                temp.add(house)
            }
        }

        _listTopRated.value = temp
    }

    private val _navigate = MutableLiveData<Event<Apartment>>()
    val navigate: LiveData<Event<Apartment>>
        get() = _navigate

    val recyclerViewItemClickCallback = object :
        RecyclerViewItemClickCallback {
        override fun onRecyclerViewItemClick(any: Any) {
            if (any is Apartment) {
                _navigate.postValue(Event(any))
            }
        }
    }

}