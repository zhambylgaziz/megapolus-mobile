package kz.megapolus.app.ui.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentReviewFormBinding
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.KeyboardUtils

class ReviewFormFragment : BaseFragment() {

    private val args: ReviewFormFragmentArgs by navArgs()

    private lateinit var binding: FragmentReviewFormBinding
    private lateinit var viewModel: ReviewFormViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_review_form,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(ReviewFormViewModel::class.java)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)
        viewModel.setArgs(userViewModel.getUser(), args.apartmentId)
        binding.viewModel = viewModel

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.reviewResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        showDialog()
                    }
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.popBackStack.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    findNavController().popBackStack()
                }
            }
        )
        viewModel.hideKeyboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    KeyboardUtils.hideKeyboard(
                        context,
                        binding.btnNext
                    )
                }
            }
        )
    }

    private fun showDialog() {
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.booking_success)
            .setMessage(R.string.review_send_text)
            .setPositiveButton(context!!.getString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
                viewModel.popBackStack()
            }
            .show()
    }

}