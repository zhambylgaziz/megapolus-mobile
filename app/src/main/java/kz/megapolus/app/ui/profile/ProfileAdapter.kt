package kz.megapolus.app.ui.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.databinding.AdapterProfileBinding
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback

class ProfileAdapter(
    var recyclerViewItemClickCallback: RecyclerViewItemClickCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            return when {
                oldItem is TitleValue && newItem is TitleValue -> {
                    oldItem.id == newItem.id
                }
                else -> {
                    false
                }
            }
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            return when {
                oldItem is TitleValue && newItem is TitleValue -> {
                    oldItem as TitleValue == newItem as TitleValue
                }
                else -> {
                    false
                }
            }
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list: List<Any>) {
        differ.submitList(list)
    }

    companion object {
        private const val VIEW_TYPE_TITLE_VALUE = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_TITLE_VALUE -> {
                val binding: AdapterProfileBinding =
                    DataBindingUtil.inflate(inflater, R.layout.adapter_profile, parent, false)
                TitleValueViewHolder(binding)
            }
            else -> {
                throw IllegalStateException("Incorrect ViewType found")
            }
        }
    }

    inner class TitleValueViewHolder(var binding: AdapterProfileBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initContent(titleValue: TitleValue) {
            binding.item = titleValue
            binding.recyclerViewItemClickCallback = recyclerViewItemClickCallback
            binding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_TITLE_VALUE -> {
                val viewHolder = holder as TitleValueViewHolder
                viewHolder.initContent(differ.currentList[position] as TitleValue)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (differ.currentList[position]) {
            is TitleValue -> VIEW_TYPE_TITLE_VALUE
            else -> throw IllegalStateException("Incorrect ViewType found")
        }

}