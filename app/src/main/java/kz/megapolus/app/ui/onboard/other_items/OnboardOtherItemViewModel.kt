package kz.megapolus.app.ui.onboard.other_items

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kz.megapolus.app.R
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class OnboardOtherItemViewModel
@Inject constructor(
    private val app: Application
) : AndroidViewModel(app) {

    val iconField = MutableLiveData<Int>()
    val titleFieldText = MutableLiveData<Int>()
    val descriptionFieldText = MutableLiveData<Int>()

    fun setPosition(position: Int) {
        when (position) {
            0 -> {
                iconField.postValue(R.drawable.ic_onboarding_explore)
                titleFieldText.postValue(R.string.onboarding_explore)
                descriptionFieldText.postValue(R.string.onboarding_explore_text)
            }
            1 -> {
                iconField.postValue(R.drawable.ic_onboarding_agent)
                titleFieldText.postValue(R.string.onboarding_agent)
                descriptionFieldText.postValue(R.string.onboarding_agent_text)
            }
            2 -> {
                iconField.postValue(R.drawable.ic_onboarding_neighborhood)
                titleFieldText.postValue(R.string.onboarding_neighboarhood)
                descriptionFieldText.postValue(R.string.onboarding_neighboarhood_text)
            }
            3 -> {
                iconField.postValue(R.drawable.ic_onboarding_renter)
                titleFieldText.postValue(R.string.onboarding_buyer)
                descriptionFieldText.postValue(R.string.onboarding_buyer_text)
            }
            4 -> {
                iconField.postValue(R.drawable.ic_onboarding_message)
                titleFieldText.postValue(R.string.onboarding_message)
                descriptionFieldText.postValue(R.string.onboarding_message_text)
            }
        }
    }

}