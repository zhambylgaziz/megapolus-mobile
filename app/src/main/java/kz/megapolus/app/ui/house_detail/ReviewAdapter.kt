package kz.megapolus.app.ui.house_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Review
import kz.megapolus.app.databinding.AdapterReviewBinding
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback

class ReviewAdapter(
    private val recyclerViewItemClickCallback: RecyclerViewItemClickCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Review>() {

        override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean =
            oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list: List<Review>) {
        differ.submitList(list)
    }

    companion object {
        private const val VIEW_TYPE_REVIEW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_REVIEW -> {
                val binding: AdapterReviewBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.adapter_review,
                        parent,
                        false
                    )
                ReviewViewHolder(binding)
            }
            else -> {
                throw IllegalStateException("Incorrect ViewType found")
            }
        }
    }

    inner class ReviewViewHolder(var binding: AdapterReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initContent(review: Review) {
            binding.review = review
            binding.recyclerViewItemClickCallback = recyclerViewItemClickCallback
            binding.executePendingBindings()
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_REVIEW -> {
                val viewHolder = holder as ReviewViewHolder
                viewHolder.initContent(differ.currentList[position] as Review)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (differ.currentList[position]) {
            is Review -> VIEW_TYPE_REVIEW
            else -> throw IllegalStateException("Incorrect ViewType found")
        }

}