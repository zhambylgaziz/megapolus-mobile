package kz.megapolus.app.ui.activities.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kz.megapolus.app.data.auth_state.AuthState
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    private val app: Application,
    private val authState: AuthState
) : AndroidViewModel(app) {

    val addButtonVisible = MutableLiveData<Boolean>()

    init {
        updateButtonVisibility()
    }

    fun updateButtonVisibility() {
        val visible = authState.isAuthorized
        addButtonVisible.postValue(visible)
    }

    fun isAuthorized(): Boolean = authState.isAuthorized

    private val _addApartment = MutableLiveData<Event<Unit>>()
    val addApartment: LiveData<Event<Unit>>
        get() = _addApartment

    fun onAddButtonClick() {
        _addApartment.postValue(Event(Unit))
    }

}