package kz.megapolus.app.ui.profile.my_bookings.detail.booking_status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentBookingStatusBinding
import kz.megapolus.app.ui.profile.my_bookings.detail.BookingDetailViewPagerFragment
import kz.megapolus.app.ui.profile.my_bookings.detail.BookingDetailViewPagerFragmentDirections
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class BookingStatusFragment : BaseFragment() {

    private lateinit var binding: FragmentBookingStatusBinding
    private lateinit var viewModel: BookingStatusViewModel
    private lateinit var userViewModel: UserViewModel

    companion object {
        fun newInstance(booking: Booking): BookingStatusFragment {
            val args = Bundle()
            args.putParcelable("booking", booking)

            val fragment = BookingStatusFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_booking_status,
                container,
                false
            )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(BookingStatusViewModel::class.java)
        viewModel.setArgs(
            booking = arguments!!.getParcelable<Booking>("booking") as Booking,
            user = userViewModel.getUser()
        )

        binding.viewModel = viewModel

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.bookingResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                    Status.ERROR -> {
                        handleExceptionView(it.exception)
                    }
                    Status.LOADING -> {
                        //
                    }
                }
            }
        )
        viewModel.statusResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        viewModel.onStatusSuccess()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionView(it.exception)
                    }
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                }
            }
        )
        viewModel.writeReview.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val booking = arguments!!.getParcelable<Booking>("booking") as Booking
                    booking.apartment?.let { apartmentId ->
                        val action =
                            BookingDetailViewPagerFragmentDirections.actionBookingDetailViewPagerFragmentToReviewFormFragment(
                                apartmentId = apartmentId
                            )
                        findNavController().navigate(
                            action,
                            getSlideLeftAnimBuilder().build()
                        )
                    }
                }
            }
        )
    }

}