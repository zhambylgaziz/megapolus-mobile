package kz.megapolus.app.ui.saved

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.house.Facility
import kz.megapolus.app.data.models.house.House
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.ui_common.callbacks.SwipeToRefreshCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class SavedViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository,
    private val preferences: Preferences

) : AndroidViewModel(app) {

    private val _refresh = MutableLiveData<Unit>()

    init {
        _refresh.postValue(Unit)
    }

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     *
     */
    var isRefreshing = MutableLiveData<Boolean>()

    init {
        isRefreshing.value = false
    }

    val swipeToRefreshCallback = object :
        SwipeToRefreshCallback {
        override fun onSwipeToRefresh() {
            _refresh.value = Unit
            isRefreshing.value = false
        }
    }

    /**
     * Network
     */
    val apartmentsResource: LiveData<Resource<List<Apartment>>> =
        Transformations.switchMap(_refresh) {
            repository.getApartments()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(apartmentsResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(apartmentsResource) {
            it.status
        }

    /**
     * Content
     */
    private val _list = MutableLiveData<List<Apartment>>()
    val list: LiveData<List<Apartment>>
        get() = _list

    fun onSuccess(list: List<Apartment>?) {
        val temp = mutableListOf<Apartment>()
        val savedList = preferences.getSavedList()

        list?.forEach { apartment ->
            savedList.forEach { saved ->
                if (saved == apartment.id) {
                    temp.add(apartment)
                }
            }
        }

        _list.postValue(temp)
    }


    /**
     *
     */
    private val _navigate = MutableLiveData<Event<Apartment>>()
    val navigate: LiveData<Event<Apartment>>
        get() = _navigate

    val recyclerViewItemClickCallback = object :
        RecyclerViewItemClickCallback {
        override fun onRecyclerViewItemClick(any: Any) {
            if (any is Apartment) {
                _navigate.postValue(Event(any))
            }
        }
    }

}