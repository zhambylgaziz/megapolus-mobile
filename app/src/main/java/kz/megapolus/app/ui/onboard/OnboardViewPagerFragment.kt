package kz.megapolus.app.ui.onboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import kz.megapolus.app.R
import kz.megapolus.app.databinding.FragmentOnboardViewPagerBinding
import kz.megapolus.app.ui.activities.main.MainActivity
import kz.megapolus.app.ui_common.base.BaseFragment

class OnboardViewPagerFragment : BaseFragment() {

    private lateinit var binding: FragmentOnboardViewPagerBinding
    private lateinit var viewModel: OnboardViewPagerViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_onboard_view_pager,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pagerAdapter = OnboardViewPagerAdapter(childFragmentManager, arrayListOf(0, 1, 2, 3, 4))
        binding.viewPager.adapter = pagerAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(OnboardViewPagerViewModel::class.java)
        binding.viewModel = viewModel

        binding.viewPager.addOnPageChangeListener(viewModel.onPageChangeCallback)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.setCurrentItem.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    binding.viewPager.setCurrentItem(it, true)
                }
            }
        )

        viewModel.navigateNext.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {

                    val intent = MainActivity.getIntent(context)
                    startActivity(intent)

                    activity?.finish()
                }
            }
        )
    }

}