package kz.megapolus.app.ui.profile.my_apartments

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.ui_common.callbacks.SwipeToRefreshCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class MyApartmentsViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private var user: User? = null

    fun setArgs(user: User?) {
        this.user = user
        _refresh.postValue(Unit)
    }

    private val _refresh = MutableLiveData<Unit>()

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     *
     */
    var isRefreshing = MutableLiveData<Boolean>()

    init {
        isRefreshing.value = false
    }

    val swipeToRefreshCallback = object :
        SwipeToRefreshCallback {
        override fun onSwipeToRefresh() {
            _refresh.value = Unit
            isRefreshing.value = false
        }
    }

    /**
     * Network
     */
    val apartmentsResource: LiveData<Resource<List<Apartment>>> =
        Transformations.switchMap(_refresh) {
            repository.getApartments()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(apartmentsResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(apartmentsResource) {
            it.status
        }

    private val deleteApartment = MutableLiveData<String>()

    val deleteResource: LiveData<Resource<Unit>> =
        Transformations.switchMap(deleteApartment) {
            repository.deleteApartment(it)
        }

    /**
     * Content
     */
    private val _list = MutableLiveData<List<Apartment>>()
    val list: LiveData<List<Apartment>>
        get() = _list

    fun onSuccess(list: List<Apartment>?) {
        val temp = mutableListOf<Apartment>()

        list?.let {
            it.forEach { apartment ->
                if (user?.id == apartment.user) {
                    var total = 0.0
                    val size = if (!apartment.reviews.isNullOrEmpty()) {
                        apartment.reviews.size
                    } else 1
                    apartment.reviews.forEach { review ->
                        review.star?.let { star ->
                            total += star
                        }
                    }
                    apartment.review = (total / size).toString()
                    temp.add(apartment)
                }
            }
        }

        _list.postValue(temp)
    }

    /**
     *
     */
    private val _navigate = MutableLiveData<Event<Apartment>>()
    val navigate: LiveData<Event<Apartment>>
        get() = _navigate

    private val _editApartment = MutableLiveData<Event<Apartment>>()
    val editApartment: LiveData<Event<Apartment>>
        get() = _editApartment


    private var selectedApartment: Apartment? = null

    private var _optionsList: MutableList<TitleValue> = mutableListOf()

    private val _openOptionsDialog = MutableLiveData<Event<List<TitleValue>>>()
    val openOptionsDialog: LiveData<Event<List<TitleValue>>>
        get() = _openOptionsDialog

    private val _deleteDialog = MutableLiveData<Event<Unit>>()
    val deleteDialog: LiveData<Event<Unit>>
        get() = _deleteDialog

    fun onDeleteConfirm() {
        selectedApartment?.id?.let {
            deleteApartment.postValue(it)
        }
    }

    fun onDeleteSuccess() {
        _refresh.postValue(Unit)
        selectedApartment = null
    }

    init {
        _optionsList.add(
            TitleValue(
                id = RequestCodeEnums.EDIT.name,
                title = R.string.apartments_edit
            )
        )

        _optionsList.add(
            TitleValue(
                id = RequestCodeEnums.DELETE.name,
                title = R.string.apartments_delete
            )
        )
    }

    fun onOptionSelected(titleValue: TitleValue?) {
        when (titleValue?.id) {
            RequestCodeEnums.EDIT.name -> {
                selectedApartment?.let {
                    _editApartment.postValue(Event(it))
                }
                selectedApartment = null
            }
            RequestCodeEnums.DELETE.name -> {
                _deleteDialog.postValue(Event(Unit))
            }
        }
    }

    val recyclerViewItemClickCallback = object :
        RecyclerViewItemClickCallback {
        override fun onRecyclerViewItemClick(any: Any) {
            if (any is Apartment) {
                _navigate.postValue(Event(any))
            }
        }
    }
    val apartmentsCallback = object : MyApartmentsCallback {
        override fun onMoreItemClick(any: Any) {
            if (any is Apartment) {
                selectedApartment = any
                _openOptionsDialog.postValue(Event(_optionsList))
            }
        }
    }
}