package kz.megapolus.app.ui.profile.my_bookings.detail.booking_apartment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import javax.inject.Inject

class BookingApartmentViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private lateinit var apartment: Apartment
    private lateinit var apartmentId: String
    private var user: User? = null

    fun setArgs(apartmentId: String, user: User?) {
        this.apartmentId = apartmentId
        this.user = user
        _refresh.postValue(Unit)
    }

    private val _refresh = MutableLiveData<Unit>()

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     * Network
     */
    val apartmentResource: LiveData<Resource<Apartment>> =
        Transformations.switchMap(_refresh) {
            repository.getApartment(apartmentId)
        }

    val resource: LiveData<Resource<Any>>?
        get() = Transformations.map(apartmentResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(apartmentResource) {
            it.status
        }

    fun onSuccess(apartment: Apartment?){
        apartment?.let {
            this.apartment = it
        }
    }
}