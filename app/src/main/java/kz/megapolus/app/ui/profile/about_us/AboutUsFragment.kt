package kz.megapolus.app.ui.profile.about_us

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import kz.megapolus.app.R
import kz.megapolus.app.databinding.FragmentAboutUsBinding
import kz.megapolus.app.ui_common.base.BaseFragment

class AboutUsFragment : BaseFragment() {

    private lateinit var binding: FragmentAboutUsBinding
    private lateinit var viewModel: AboutUsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_about_us,
                container,
                false
            )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(AboutUsViewModel::class.java)

        binding.viewModel = viewModel
        binding.tvHome.movementMethod = LinkMovementMethod.getInstance()

        observeViewModel()
    }

    private fun observeViewModel() {
    }

}