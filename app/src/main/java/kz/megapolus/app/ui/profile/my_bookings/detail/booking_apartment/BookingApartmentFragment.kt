package kz.megapolus.app.ui.profile.my_bookings.detail.booking_apartment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentBookingApartmentBinding
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment

class BookingApartmentFragment : BaseFragment() {

    private lateinit var binding: FragmentBookingApartmentBinding
    private lateinit var viewModel: BookingApartmentViewModel
    private lateinit var userViewModel: UserViewModel

    companion object {
        fun newInstance(apartmentId: String): BookingApartmentFragment {
            val args = Bundle()
            args.putString("apartmentId", apartmentId)

            val fragment = BookingApartmentFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_booking_apartment,
                container,
                false
            )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(BookingApartmentViewModel::class.java)
        viewModel.setArgs(
            apartmentId = arguments!!.getString("apartmentId") as String,
            user = userViewModel.getUser()
        )

        binding.viewModel = viewModel

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apartmentResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                        binding.apartment = it.data
                    }
                    Status.ERROR -> {
                        handleExceptionView(it.exception)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        )
    }

}