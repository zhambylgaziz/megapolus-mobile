package kz.megapolus.app.ui.login

import kz.megapolus.app.ui_common.base.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentLoginBinding
import kz.megapolus.app.ui.activities.main.MainViewModel
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.utils.KeyboardUtils
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class LoginFragment : BaseFragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(LoginViewModel::class.java)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)
        mainViewModel = getViewModelOfActivity(MainViewModel::class.java)
        binding.viewModel = viewModel

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.authResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        viewModel.onSuccess(it.data)
                        userViewModel.onUserSuccess()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.updateButton.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    mainViewModel.updateButtonVisibility()
                    findNavController().popBackStack(R.id.profileFragment, false)
                }
            }
        )
        viewModel.openPasswordReset.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {

                }
            }
        )
        viewModel.openRegistration.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        LoginFragmentDirections.actionLoginFragmentToNavigatioRegistration()
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.hideKeyboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    KeyboardUtils.hideKeyboard(
                        context,
                        binding.btnLogin
                    )
                }
            }
        )
    }

}