package kz.megapolus.app.ui.onboard

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kz.megapolus.app.ui.onboard.other_items.OnboardOtherItemFragment

class OnboardViewPagerAdapter(fragmentManager: FragmentManager, private val items: ArrayList<Int>) :
    FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return OnboardOtherItemFragment.newInstance(items[position])
        }

    override fun getCount(): Int {
        return items.size
    }

}