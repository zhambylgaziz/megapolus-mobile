package kz.megapolus.app.ui.saved

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentSavedBinding
import kz.megapolus.app.ui.explore.ApartmensAdapter
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class SavedFragment : BaseFragment() {

    private lateinit var binding: FragmentSavedBinding
    private lateinit var viewModel: SavedViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_saved, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel(SavedViewModel::class.java)
        binding.viewModel = viewModel

        binding.recyclerView.adapter = ApartmensAdapter(viewModel.recyclerViewItemClickCallback)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apartmentsResource.observe(
            viewLifecycleOwner,
            Observer {
                when(it.status){
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                }
            }
        )
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.recyclerView.adapter as ApartmensAdapter).submitList(it)
            }
        )

        viewModel.navigate.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        SavedFragmentDirections.actionSavedFragmentToNavigationHouseDetail(it)
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
    }
}