package kz.megapolus.app.ui.profile.my_apartments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.databinding.AdapterApartmentBinding
import kz.megapolus.app.databinding.AdapterMyApartmentsBinding
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback

class MyApartmentsAdapter(
    private val recyclerViewItemClickCallback: RecyclerViewItemClickCallback,
    private val myApartmentsCallback: MyApartmentsCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Apartment>() {

        override fun areItemsTheSame(oldItem: Apartment, newItem: Apartment): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: Apartment, newItem: Apartment): Boolean =
            oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list: List<Apartment>) {
        differ.submitList(list)
    }

    companion object {
        private const val VIEW_TYPE_APARTMENT = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_APARTMENT -> {
                val binding: AdapterMyApartmentsBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.adapter_my_apartments,
                        parent,
                        false
                    )
                SavedViewHolder(binding)
            }
            else -> {
                throw IllegalStateException("Incorrect ViewType found")
            }
        }
    }

    inner class SavedViewHolder(var binding: AdapterMyApartmentsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initContent(house: Apartment) {
            binding.house = house
            binding.recyclerViewItemClickCallback = recyclerViewItemClickCallback
            binding.myApartmentsCallback = myApartmentsCallback
            binding.ivImage.clipToOutline = true
            binding.executePendingBindings()
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_APARTMENT -> {
                val viewHolder = holder as SavedViewHolder
                viewHolder.initContent(differ.currentList[position] as Apartment)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (differ.currentList[position]) {
            is Apartment -> VIEW_TYPE_APARTMENT
            else -> throw IllegalStateException("Incorrect ViewType found")
        }

}