package kz.megapolus.app.ui.review

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class ReviewFormViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private var user: User? = null
    private var apartmentId: String? = null

    fun setArgs(user: User?, apartmentId: String?) {
        this.user = user
        this.apartmentId = apartmentId
    }

    /**
     * Rating
     */
    val rating = MutableLiveData<Float>()

    /**
     * Message
     */
    val messageFieldText = MutableLiveData<String>()
    val messageFieldError = MutableLiveData<Any>()

    fun onMessageFieldTextChanged() {
        if (!messageFieldText.value.isNullOrEmpty()) {
            messageFieldError.postValue(null)
            nextButtonEnabled.postValue(true)
        } else {
            nextButtonEnabled.postValue(false)
        }
    }

    val nextButtonEnabled = MutableLiveData<Boolean>()

    init {
        nextButtonEnabled.postValue(false)
    }

    fun onNextBtnClick() {
        _hideKeyboard.postValue(Event(Unit))
        add.postValue(Unit)
    }

    /**
     * Network
     */
    private val add = MutableLiveData<Unit>()

    val reviewResource =
        Transformations.switchMap(add) {
            val map = mutableMapOf<String, Any>()
            map["user"] = user?.id ?: "0"
            map["star"] = rating.value ?: 0.0
            map["apartment"] = apartmentId ?: "0"
            map["name"] = user?.username ?: "Неизвестный"
            map["review"] = messageFieldText.value ?: ""
            repository.postReview(map)
        }

    /**
     * Other
     */
    private val _hideKeyboard = MutableLiveData<Event<Unit>>()
    val hideKeyboard: LiveData<Event<Unit>>
        get() = _hideKeyboard

    private val _popBackStack = MutableLiveData<Event<Unit>>()
    val popBackStack: LiveData<Event<Unit>>
        get() = _popBackStack

    fun popBackStack() {
        _popBackStack.postValue(Event(Unit))
    }

}