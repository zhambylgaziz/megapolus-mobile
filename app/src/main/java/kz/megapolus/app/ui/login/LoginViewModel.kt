package kz.megapolus.app.ui.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.auth_state.AuthState
import kz.megapolus.app.data.enums.app.OtpTypeEnums
import kz.megapolus.app.data.enums.app.auth.AuthTypeEnums
import kz.megapolus.app.data.models.auth.Token
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.auth.AuthRepository
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class LoginViewModel
@Inject constructor(
    private val app: Application,
    private val repository: AuthRepository,
    private val preferences: Preferences
) : AndroidViewModel(app) {

    /**
     * Login
     */
    val loginFieldText = MutableLiveData<String>()
    val loginFieldError = MutableLiveData<Any>()

    fun onLoginFieldTextChanged() {
        if (!loginFieldText.value.isNullOrEmpty()) {
            loginFieldError.postValue(null)
        }
    }

    /**
     * Password
     */
    val passwordFieldText = MutableLiveData<String>()
    val passwordFieldError = MutableLiveData<Any>()

    fun onPasswordFieldTextChanged() {
        if (!passwordFieldText.value.isNullOrEmpty()) {
            passwordFieldError.postValue(null)
        }
        checkFields()
    }

    val loginBtnEnabled = MutableLiveData<Boolean>()

    init {
        loginBtnEnabled.value = false
    }

    private fun checkFields() {
        if (!passwordFieldText.value.isNullOrBlank()
            && !loginFieldText.value.isNullOrBlank()
        ) {
            loginBtnEnabled.postValue(true)
        } else {
            loginBtnEnabled.postValue(false)
        }
    }

    /**
     *  Login Button
     */
    fun onLoginBtnClick() {
        _hideKeyboard.postValue(Event(Unit))

        val loginNotEmpty = !loginFieldText.value.isNullOrEmpty()
        val passwordNotEmpty = !passwordFieldText.value.isNullOrEmpty()

        if (
            loginNotEmpty
            && passwordNotEmpty
        ) {
            signInBtnClick.postValue(Unit)
        } else {
            if (!loginNotEmpty) {
                loginFieldError.postValue(R.string.field_error_empty)
            }
            if (!passwordNotEmpty) {
                passwordFieldError.postValue(R.string.field_error_empty)
            }
        }

    }

    /**
     * Network
     */
    private val signInBtnClick = MutableLiveData<Unit>()

    val authResource: LiveData<Resource<Token>> =
        Transformations.switchMap(signInBtnClick) {
            val map = mapOf(
                "username" to loginFieldText.value!!,
                "password" to passwordFieldText.value!!
            )
            repository.authLogin(map)
        }

    fun onSuccess(token: Token?) {
        token?.authToken?.let {
            preferences.setAppToken(it)
            preferences.setAuthType(AuthTypeEnums.PASSWORD.id)
            _updateButton.postValue(Event(Unit))
        }
    }

    private val _updateButton = MutableLiveData<Event<Unit>>()
    val updateButton: LiveData<Event<Unit>>
        get() = _updateButton

    /**
     * Forgot
     */
    private val _openPasswordReset = MutableLiveData<Event<Unit>>()
    val openPasswordReset: LiveData<Event<Unit>>
        get() = _openPasswordReset

    fun onForgotPasswordBtnClick() {
        _openPasswordReset.postValue(Event(Unit))
    }

    /**
     * Registration
     */
    private val _openRegistration = MutableLiveData<Event<Unit>>()
    val openRegistration: LiveData<Event<Unit>>
        get() = _openRegistration

    fun onRegistrationBtnClick() {
        _openRegistration.postValue(Event(Unit))
    }

    /**
     * Other
     */
    private val _hideKeyboard = MutableLiveData<Event<Unit>>()
    val hideKeyboard: LiveData<Event<Unit>>
        get() = _hideKeyboard
}