package kz.megapolus.app.ui.splash

import android.app.Application
import android.os.CountDownTimer
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kz.megapolus.app.data.enums.app.auth.AuthTypeEnums
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class SplashViewModel
@Inject constructor(
    private val app: Application,
    private val preferences: Preferences
) : AndroidViewModel(app) {

    init {
        GlobalScope.launch(Dispatchers.Main) {
            openNext()
        }
    }

    private suspend fun openNext() {
        delay(1500)

        if (preferences.isOnboardShown()) {
//            if (shouldOpenPIN()) {
//                _openCheckPIN.postValue(Event(Unit))
//            } else {
                _openLogin.postValue(Event(Unit))
//            }

        } else {
            _openOnboard.postValue(Event(Unit))
        }
    }

    private fun shouldOpenPIN(): Boolean {
        return when (preferences.getAuthType()) {
            AuthTypeEnums.PIN.id,
            AuthTypeEnums.BIOMETRIC.id -> {
                true
            }
            else -> {
                false
            }
        }
    }

    private val _openOnboard = MutableLiveData<Event<Unit>>()
    val openOnboard: LiveData<Event<Unit>>
        get() = _openOnboard

    private val _openLogin = MutableLiveData<Event<Unit>>()
    val openLogin: LiveData<Event<Unit>>
        get() = _openLogin

    private val _openCheckPIN = MutableLiveData<Event<Unit>>()
    val openCheckPIN: LiveData<Event<Unit>>
        get() = _openCheckPIN

}