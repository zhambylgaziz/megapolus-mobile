package kz.megapolus.app.ui.onboard.other_items

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import kz.megapolus.app.R
import kz.megapolus.app.databinding.FragmentOnboardOtherItemBinding
import kz.megapolus.app.ui_common.base.BaseFragment

class OnboardOtherItemFragment : BaseFragment() {

    private lateinit var binding: FragmentOnboardOtherItemBinding
    private lateinit var viewModel: OnboardOtherItemViewModel

    companion object {
        fun newInstance(position: Int): OnboardOtherItemFragment {
            var args = Bundle()
            args.putInt("position", position)

            var fragment = OnboardOtherItemFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_onboard_other_item,
                container,
                false
            )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(OnboardOtherItemViewModel::class.java)
        viewModel.setPosition(position = arguments!!.getInt("position"))
        binding.viewModel = viewModel
    }

}