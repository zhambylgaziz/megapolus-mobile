package kz.megapolus.app.ui.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentProfileBinding
import kz.megapolus.app.ui.activities.main.MainViewModel
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder
import kz.megapolus.app.utils.rv.getDividerItemDecoration


class ProfileFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_profile,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.includeProfile.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        binding.includeProfile.recyclerView.addItemDecoration(
            getDividerItemDecoration(
                context = context!!
            )
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel(ProfileViewModel::class.java)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)
        mainViewModel = getViewModelOfActivity(MainViewModel::class.java)
        binding.viewModel = viewModel

        binding.includeProfile.recyclerView.adapter =
            ProfileAdapter(viewModel.recyclerViewItemClickListener)

        observeViewModel()
        observeUserViewModel()
    }

    private fun observeViewModel() {
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.includeProfile.recyclerView.adapter as ProfileAdapter).submitList(it)
            }
        )
        viewModel.navigate.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    findNavController().navigate(
                        it,
                        Bundle.EMPTY,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.userResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.LOADING -> {
//                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
//                        dismissProgressDialog()
                        viewModel.onSuccessUser(it.data)
                        userViewModel.onSuccess(it.data)
                    }
                    Status.ERROR -> {
                        viewModel.onError()
//                        dismissProgressDialog()
//                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.logoutResource.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    when (it.status) {
                        Status.LOADING -> {
                            showProgressDialog()
                        }
                        Status.SUCCESS -> {
                            dismissProgressDialog()
                            viewModel.onSuccess()
                        }
                        Status.ERROR -> {
                            dismissProgressDialog()
                            handleExceptionDialog(it.exception)
                        }
                    }
                }
            }
        )
        viewModel.openLogin.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action = ProfileFragmentDirections.actionProfileFragmentToLogin()
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.updateButton.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    mainViewModel.updateButtonVisibility()
                }
            }
        )
        viewModel.openRegistration.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        ProfileFragmentDirections.actionProfileFragmentToNavigatioRegistration()
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.openPrivacyPolicies.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val webpage = Uri.parse(it)

                    val intent = Intent(Intent.ACTION_VIEW, webpage)

                    context?.packageManager?.let {
                        if (intent.resolveActivity(it) != null) {
                            startActivity(intent)
                        } else {
                            //Page not found
                        }
                    }
                }
            }
        )

        viewModel.showLogoutConfirm.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    showConfirmDialog(
                        messageRes = R.string.profile_logout_confirm,
                        deleteConfirmBtnClickListener = {
                            viewModel.onLogoutConfirmBtnClick()
                        }
                    )
                }
            }
        )

    }

    private fun observeUserViewModel() {
        userViewModel.refreshState.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    viewModel.refreshState()
                }
            }
        )
    }

    private fun showConfirmDialog(
        messageRes: Int,
        deleteConfirmBtnClickListener: () -> Unit
    ) {
        MaterialAlertDialogBuilder(context)
            .setMessage(messageRes)
            .setPositiveButton(
                R.string.profile_btn_yes
            ) { dialog, which -> deleteConfirmBtnClickListener.invoke() }
            .setNegativeButton(
                R.string.profile_btn_no
            ) { dialog, which -> dialog.cancel() }
            .show()
    }

}