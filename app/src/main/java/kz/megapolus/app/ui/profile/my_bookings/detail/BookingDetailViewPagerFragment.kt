package kz.megapolus.app.ui.profile.my_bookings.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import kz.megapolus.app.R
import kz.megapolus.app.databinding.FragmentBookingDetailBinding
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_apartment.BookingApartmentFragment
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_status.BookingStatusFragment
import kz.megapolus.app.ui_common.base.BaseFragment

class BookingDetailViewPagerFragment : BaseFragment() {

    val args: BookingDetailViewPagerFragmentArgs by navArgs()

    private lateinit var binding: FragmentBookingDetailBinding
    private lateinit var viewModel: BookingDetailViewPagerViewModel

    private val objects = mutableListOf<Fragment>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_booking_detail,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        objects.clear()

        objects.add(
            BookingStatusFragment.newInstance(args.booking)
        )
        objects.add(
            BookingApartmentFragment.newInstance(args.booking.apartment ?: "")
        )

        binding.viewPager.offscreenPageLimit = 2

        val pagerAdapter = BookingDetailViewPagerAdapter(childFragmentManager, objects)
        binding.viewPager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.tabLayout.getTabAt(0)
            ?.setText(R.string.bookings_detail_booking)

        binding.tabLayout.getTabAt(1)
            ?.setText(R.string.bookings_detail_apartment)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModelOfActivity(BookingDetailViewPagerViewModel::class.java)

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                //do nothing
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                //do nothing
            }

            override fun onPageSelected(position: Int) {
            }
        })

        observeViewModel()
    }

    fun observeViewModel() {
    }

}