package kz.megapolus.app.ui.booking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentBookingBinding
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.ui_common.dialogs.CustomDatePickerDialog
import kz.megapolus.app.utils.KeyboardUtils
import java.util.*

class BookingFragment : BaseFragment() {

    private val args: BookingFragmentArgs by navArgs()

    private lateinit var binding: FragmentBookingBinding
    private lateinit var viewModel: BookingViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_booking, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)
        viewModel = getViewModel(BookingViewModel::class.java)
        viewModel.setArgs(userViewModel.getUser(), args.apartment)

        binding.viewModel = viewModel
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.openSelectDateDialog.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val dialog = CustomDatePickerDialog.newInstance()
                    dialog.setTargetFragment(this, it)
                    dialog.show(fragmentManager!!, "")
                }
            }
        )
        viewModel.bookingResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        showDialog()
                    }
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.popBackStack.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    findNavController().popBackStack()
                }
            }
        )
        viewModel.hideKeyboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    KeyboardUtils.hideKeyboard(
                        context,
                        binding.btnNext
                    )
                }
            }
        )
    }

    private fun showDialog() {
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.booking_success)
            .setMessage(R.string.booking_send_text)
            .setPositiveButton(context!!.getString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
                viewModel.popBackStack()
            }
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            RequestCodeEnums.SELECT_FROM_DATE.id -> {
                data?.extras?.let {
                    val date = it.getSerializable("date") as Date
                    viewModel.onDateStartSelected(date)
                }
            }
            RequestCodeEnums.SELECT_TO_DATE.id -> {
                data?.extras?.let {
                    val date = it.getSerializable("date") as Date
                    viewModel.onDateEndSelected(date)
                }
            }
        }
    }

}