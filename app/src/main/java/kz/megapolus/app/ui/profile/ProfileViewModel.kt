package kz.megapolus.app.ui.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kz.megapolus.app.R
import kz.megapolus.app.data.auth_state.AuthState
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.auth.AuthRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class ProfileViewModel
@Inject constructor(
    private val app: Application,
    private val authState: AuthState,
    private val repository: AuthRepository,
    private val preferences: Preferences
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private val refresh = MutableLiveData<Unit>()

    val loginNotAuthVisible = MutableLiveData<Boolean>()

    private val _list = MutableLiveData<List<Any>>()
    val list: LiveData<List<Any>>
        get() = _list

    private fun initUI() {
        val temp = mutableListOf<Any>()
        temp.add(
            TitleValue(
                id = "apartments",
                title = R.string.profile_my_bookings,
                valueInt = R.id.myApartmentsFragment,
                icon = R.drawable.ic_profile_home
            )
        )
        temp.add(
            TitleValue(
                id = "bookings",
                title = R.string.profile_my_apartments,
                valueInt = R.id.myBookingsFragment,
                icon = R.drawable.ic_profile_book
            )
        )
//        temp.add(
//            TitleValue(
//                id = "edit",
//                title = R.string.profile_my_edit,
//                valueInt = R.id.profileEditFragment,
//                icon = R.drawable.ic_profile_edit
//            )
//        )
        temp.add(
            TitleValue(
                id = "about",
                title = R.string.profile_about_us,
                valueInt = R.id.aboutUsFragment,
                icon = R.drawable.ic_profile_info
            )
        )
        temp.add(
            TitleValue(
                id = "privacy",
                title = R.string.profile_privacy,
                icon = R.drawable.ic_privacy_policy
            )
        )
        temp.add(
            TitleValue(
                id = "logout",
                title = R.string.profile_btn_logout,
                icon = R.drawable.ic_profile_logout
            )
        )
        _list.postValue(temp)
    }

    init {
        refreshState()
    }

    fun refreshState() {
        val isAuthorized = authState.isAuthorized
        loginNotAuthVisible.postValue(!isAuthorized)
        if (isAuthorized) {
            refresh.postValue(Unit)
            initUI()
        }
    }

    /**
     * Profile
     */
    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            refresh.value = Unit
        }
    }

    private lateinit var user: User

    val userResource: LiveData<Resource<User>> =
        Transformations.switchMap(refresh) {
            repository.getUser()
        }

    val resource: LiveData<Resource<User>>?
        get() = Transformations.map(userResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(userResource) {
            it.status
        }

    fun onSuccessUser(user: User?) {
        user?.let {
            this.user = user
            userLogin.postValue(it.username)
        }
    }

    fun onError() {
        refreshState()
    }

    val userLogin = MutableLiveData<String>()

    private val _navigate = MutableLiveData<Event<Int>>()
    val navigate: LiveData<Event<Int>>
        get() = _navigate

    private val _openPrivacyPolicies = MutableLiveData<Event<String>>()
    val openPrivacyPolicies: LiveData<Event<String>>
        get() = _openPrivacyPolicies

    val recyclerViewItemClickListener = object :
        RecyclerViewItemClickCallback {

        override fun onRecyclerViewItemClick(any: Any) {
            when (any) {
                is TitleValue -> {
                    when (any.id) {
                        "apartments",
                        "bookings",
                        "edit",
                        "about" -> {
                            _navigate.postValue(Event(any.valueInt))
                        }
                        "privacy" -> {
                            _openPrivacyPolicies.postValue(Event("http://megapolus-kz.herokuapp.com/privacy"))
                        }
                        "logout" -> {
                            onLogoutBtnClick()
                        }
                    }
                }
            }
        }
    }

    /**
     * Logout
     */

    fun onLogoutBtnClick() {
        _showLogoutConfirm.postValue(Event(Unit))
    }

    private val _showLogoutConfirm = MutableLiveData<Event<Unit>>()
    val showLogoutConfirm: LiveData<Event<Unit>>
        get() = _showLogoutConfirm

    fun onLogoutConfirmBtnClick() {
        logout.postValue(Unit)
    }

    private val logout = MutableLiveData<Unit>()

    val logoutResource: LiveData<Event<Resource<Unit>>> =
        Transformations.switchMap(logout) {
            repository.logout()
        }

    fun onSuccess() {
        preferences.removeAppToken()
        refreshState()
        _updateButton.postValue(Event(Unit))
        _popBackStack.postValue(Event(Unit))
    }

    private val _updateButton = MutableLiveData<Event<Unit>>()
    val updateButton: LiveData<Event<Unit>>
        get() = _updateButton

    /**
     * Not Auth
     */
    private val _openLogin = MutableLiveData<Event<Unit>>()
    val openLogin: LiveData<Event<Unit>>
        get() = _openLogin

    fun onLoginBtnClick() {
        _openLogin.postValue(Event(Unit))
    }

    private val _openRegistration = MutableLiveData<Event<Unit>>()
    val openRegistration: LiveData<Event<Unit>>
        get() = _openRegistration

    fun onRegistrationBtnClick() {
        _openRegistration.postValue(Event(Unit))
    }

    private val _popBackStack = MutableLiveData<Event<Unit>>()
    val popBackStack: LiveData<Event<Unit>>
        get() = _popBackStack

}