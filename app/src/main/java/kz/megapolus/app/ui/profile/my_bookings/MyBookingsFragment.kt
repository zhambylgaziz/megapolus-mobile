package kz.megapolus.app.ui.profile.my_bookings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentMyBookingsBinding
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder
import kz.megapolus.app.utils.rv.MarginItemDecoration

class MyBookingsFragment : BaseFragment() {

    private lateinit var binding: FragmentMyBookingsBinding
    private lateinit var viewModel: MyBookingsViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_my_bookings,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        binding.recyclerView.addItemDecoration(
            MarginItemDecoration(
                context = context!!,
                topDimen = R.dimen.dp_8,
                bottomDimen = R.dimen.dp_8,
                leftDimen = R.dimen.dp_8,
                rightDimen = R.dimen.dp_8
            )
        )

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(MyBookingsViewModel::class.java)
        viewModel.setArgs(userViewModel.getUser())

        binding.viewModel = viewModel
        binding.recyclerView.adapter = MyBookingsAdapter(viewModel.recyclerViewItemClickCallback)

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.bookingResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                }
            }
        )
        viewModel.navigate.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        MyBookingsFragmentDirections.actionMyBookingsFragmentToBookingDetailViewPagerFragment(
                            it
                        )
                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.recyclerView.adapter as MyBookingsAdapter).submitList(it)
            }
        )
    }

}