package kz.megapolus.app.ui.profile.my_apartments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentMyApartmentsBinding
import kz.megapolus.app.permissions.utils.ToastUtils
import kz.megapolus.app.ui.explore.ApartmensAdapter
import kz.megapolus.app.ui.select.SelectDialog
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.navigation.getSlideLeftAnimBuilder

class MyApartmentsFragment : BaseFragment() {

    private lateinit var binding: FragmentMyApartmentsBinding
    private lateinit var viewModel: MyApartmentsViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_my_apartments,
            container,
            false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(MyApartmentsViewModel::class.java)
        viewModel.setArgs(userViewModel.getUser())

        binding.viewModel = viewModel
        binding.recyclerView.adapter = MyApartmentsAdapter(
            viewModel.recyclerViewItemClickCallback,
            viewModel.apartmentsCallback
        )

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apartmentsResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        viewModel.onSuccess(it.data)
                    }
                }
            }
        )
        viewModel.navigate.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        MyApartmentsFragmentDirections.actionMyApartmentsFragmentToNavigationHouseDetail(
                            it
                        )

                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.editApartment.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val action =
                        MyApartmentsFragmentDirections.actionMyApartmentsFragmentToNavigationApartment(
                            apartment = it
                        )

                    findNavController().navigate(
                        action,
                        getSlideLeftAnimBuilder().build()
                    )
                }
            }
        )
        viewModel.openOptionsDialog.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val dialog = SelectDialog.newInstance(it)
                    dialog.setTargetFragment(this, RequestCodeEnums.SELECT_OPTIONS.id)
                    dialog.show(fragmentManager!!, "")
                }
            }
        )
        viewModel.deleteDialog.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    showDeleteDialog()
                }
            }
        )
        viewModel.list.observe(
            viewLifecycleOwner,
            Observer {
                (binding.recyclerView.adapter as MyApartmentsAdapter).submitList(it)
            }
        )
        viewModel.deleteResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        viewModel.onDeleteSuccess()
                        ToastUtils.show(context, "Успешно удалено")
                    }
                    Status.ERROR -> {
                        handleExceptionDialog(it.exception)
                        dismissProgressDialog()
                    }
                }
            }
        )
    }

    private fun showDeleteDialog() {
        val alertDialog = MaterialAlertDialogBuilder(context)
        alertDialog.setTitle(R.string.dialog_delete_title)
        alertDialog.setMessage(R.string.dialog_delete_message_apartment)
        alertDialog.setPositiveButton(
            R.string.btn_yes
        ) { _, _ -> viewModel.onDeleteConfirm() }

        alertDialog.setNegativeButton(
            R.string.btn_no
        ) { dialog, _ -> dialog.cancel() }

        alertDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            RequestCodeEnums.SELECT_OPTIONS.id -> {
                data?.extras?.let {
                    val selectedOption: TitleValue? = it.getParcelable("selectedValue")
                    viewModel.onOptionSelected(selectedOption)
                }
            }
        }
    }

}