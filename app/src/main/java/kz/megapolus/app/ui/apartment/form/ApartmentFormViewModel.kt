package kz.megapolus.app.ui.apartment.form

import android.app.Application
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.ApartmentTypeEnums
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.custom.SelectType
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class ApartmentFormViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private var user: User? = null
    private var apartment: Apartment? = null

    val titleFieldText = MutableLiveData<Any>()
    val enabledField = MutableLiveData<Boolean>()
    val enabledFieldVisible = MutableLiveData<Boolean>()

    init {
        titleFieldText.postValue(R.string.apartments_create)
        enabledFieldVisible.postValue(false)
    }

    fun setArgs(user: User?, apartment: Apartment?) {
        this.user = user
        this.apartment = apartment
        if (apartment != null) {
            priceFieldText.postValue(apartment.price)
            districtFieldText.postValue(apartment.district)
            streetFieldText.postValue(apartment.street)
            houseFieldText.postValue(apartment.house)
            apartmentFieldText.postValue(apartment.apartment)
            apartment.flat?.let {
                _selectedFlat.postValue(Event(it.toInt()))
            }
            apartment.numberOfFloorsOfTheHouse?.let {
                _selectedAllFlat.postValue(Event(it.toInt()))
            }
            areaFieldText.postValue(apartment.area)
            roomsFieldText.postValue(apartment.rooms)
            yearFieldText.postValue(apartment.yearOfConstruction)
            descriptionFieldText.postValue(apartment.description)
            bookingEnabled.postValue(apartment.bookingEnabled)
            enabledField.postValue(apartment.enabled)
            enabledFieldVisible.postValue(true)
            titleFieldText.postValue(R.string.apartments_title_edit)
        }
    }

    /**
     *  Type
     */
    private val _openSelectDialog = MutableLiveData<Event<SelectType>>()
    val openSelectDialog: LiveData<Event<SelectType>>
        get() = _openSelectDialog

    private var type: String? = null
    val typeFieldText = MutableLiveData<String>()
    val typeFieldError = MutableLiveData<String>()

    fun onTypeFieldClick() {
        val temp = mutableListOf<Any>()
        temp.add(
            TitleValue(
                id = ApartmentTypeEnums.HOURLY.id,
                title = R.string.apartments_term_hourly
            )
        )
        temp.add(
            TitleValue(
                id = ApartmentTypeEnums.DAILY.id,
                title = R.string.apartments_term_daily
            )
        )
        temp.add(
            TitleValue(
                id = ApartmentTypeEnums.MONTHLY.id,
                title = R.string.apartments_term_monthly
            )
        )
        temp.add(
            TitleValue(
                id = ApartmentTypeEnums.QUARTERLY.id,
                title = R.string.apartments_term_quarterly
            )
        )
        _openSelectDialog.postValue(
            Event(
                SelectType(
                    RequestCodeEnums.SELECT_TYPE,
                    temp
                )
            )
        )
    }

    fun onTypeFieldSelected(titleValue: TitleValue?) {
        titleValue?.let {
            type = titleValue.id
            typeFieldText.postValue(app.getString(titleValue.title))
            typeFieldError.postValue(null)
        }
    }

    /**
     *  Price
     */
    val priceFieldText = MutableLiveData<String>()
    val priceFieldError = MutableLiveData<Any>()

    fun onPriceFieldTextChanged() {
        if (!priceFieldText.value.isNullOrEmpty()) {
            priceFieldError.postValue(null)
        }
    }

    /**
     *  District
     */
    private var district: String? = null
    val districtFieldText = MutableLiveData<String>()
    val districtFieldError = MutableLiveData<String>()

    fun onDistrictFieldClick() {
        val temp = mutableListOf<Any>()
        temp.add(
            TitleValue(
                id = "1",
                title = R.string.apartments_district_alatau
            )
        )
        temp.add(
            TitleValue(
                id = "2",
                title = R.string.apartments_district_almaly
            )
        )
        temp.add(
            TitleValue(
                id = "3",
                title = R.string.apartments_district_auezov
            )
        )
        temp.add(
            TitleValue(
                id = "4",
                title = R.string.apartments_district_bostandyk
            )
        )
        temp.add(
            TitleValue(
                id = "5",
                title = R.string.apartments_district_zhetisu
            )
        )
        temp.add(
            TitleValue(
                id = "6",
                title = R.string.apartments_district_medeu
            )
        )
        temp.add(
            TitleValue(
                id = "7",
                title = R.string.apartments_district_nauryzbay
            )
        )
        temp.add(
            TitleValue(
                id = "8",
                title = R.string.apartments_district_turkisib
            )
        )
        _openSelectDialog.postValue(
            Event(
                SelectType(
                    RequestCodeEnums.SELECT_DISTRICT,
                    temp
                )
            )
        )
    }

    fun onDistrictFieldSelected(titleValue: TitleValue?) {
        titleValue?.let {
            district = titleValue.id
            districtFieldText.postValue(app.getString(titleValue.title))
            districtFieldError.postValue(null)
        }
    }

    /**
     *  Street
     */
    val streetFieldText = MutableLiveData<String>()
    val streetFieldError = MutableLiveData<Any>()

    fun onStreetFieldTextChanged() {
        if (!streetFieldText.value.isNullOrEmpty()) {
            streetFieldError.postValue(null)
        }
    }

    /**
     *  House
     */
    val houseFieldText = MutableLiveData<String>()
    val houseFieldError = MutableLiveData<Any>()

    fun onHouseFieldTextChanged() {
        if (!houseFieldText.value.isNullOrEmpty()) {
            houseFieldError.postValue(null)
        }
    }

    /**
     * Apartment
     */
    val apartmentFieldText = MutableLiveData<String>()
    val apartmentFieldError = MutableLiveData<Any>()

    fun onApartmentFieldTextChanged() {
        if (!apartmentFieldText.value.isNullOrEmpty()) {
            apartmentFieldError.postValue(null)
        }
    }


    /**
     * Spinner
     */
    var numbers = IntRange(1, 40).toList().toList()
    var flat: Int = 1
    private val _selectedFlat = MutableLiveData<Event<Int>>()
    val selectedFlat: LiveData<Event<Int>>
        get() = _selectedFlat

    var allFloors: Int = 1
    private val _selectedAllFlat = MutableLiveData<Event<Int>>()
    val selectedAllFlat: LiveData<Event<Int>>
        get() = _selectedAllFlat

    val flatSpinnerListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
            this@ApartmentFormViewModel.flat = numbers[pos]
        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            // Another interface callback
        }
    }
    val floorsSpinnerListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
            this@ApartmentFormViewModel.allFloors = numbers[pos]
        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            // Another interface callback
        }
    }

    /**
     *  Area
     */
    val areaFieldText = MutableLiveData<String>()
    val areaFieldError = MutableLiveData<Any>()

    fun onAreaFieldTextChanged() {
        if (!areaFieldText.value.isNullOrEmpty()) {
            areaFieldError.postValue(null)
        }
    }

    /**
     *  Number of Rooms
     */
    val roomsFieldText = MutableLiveData<String>()
    val roomsFieldError = MutableLiveData<Any>()

    fun onRoomsFieldTextChanged() {
        if (!roomsFieldText.value.isNullOrEmpty()) {
            roomsFieldError.postValue(null)
        }
    }

    /**
     *  Year
     */
    val yearFieldText = MutableLiveData<String>()
    val yearFieldError = MutableLiveData<Any>()

    fun onYearFieldTextChanged() {
        if (!yearFieldText.value.isNullOrEmpty()) {
            yearFieldError.postValue(null)
        }
    }

    /**
     *  Description
     */
    val descriptionFieldText = MutableLiveData<String>()
    val descriptionFieldError = MutableLiveData<Any>()

    fun onDescriptionFieldTextChanged() {
        if (!descriptionFieldText.value.isNullOrEmpty()) {
            descriptionFieldError.postValue(null)
        }
    }

    /**
     *  Booking
     */
    val bookingEnabled = MutableLiveData<Boolean>()

    /**
     * Next Btn
     */
    private val _openNext = MutableLiveData<Event<Apartment>>()
    val openNext: LiveData<Event<Apartment>>
        get() = _openNext

    private fun checkFields(): Boolean {
        val typeSelected = !type.isNullOrBlank()
        val priceNotEmpty = !priceFieldText.value.isNullOrBlank()
        val districtNotEmpty = !districtFieldText.value.isNullOrBlank()
        val streetNotEmpty = !streetFieldText.value.isNullOrBlank()
        val houseNotEmpty = !houseFieldText.value.isNullOrBlank()
        val areaNotEmpty = !areaFieldText.value.isNullOrBlank()
        val yearNotEmpty = !yearFieldText.value.isNullOrBlank()
        val descriptionNotEmpty = !descriptionFieldText.value.isNullOrBlank()
        val roomsNotEmpty = !roomsFieldText.value.isNullOrBlank()

        if (!typeSelected) typeFieldError.postValue(app.getString(R.string.field_error_empty))
        if (!priceNotEmpty) priceFieldError.postValue(R.string.field_error_empty)
        if (!districtNotEmpty) districtFieldError.postValue(app.getString(R.string.field_error_empty))
        if (!streetNotEmpty) streetFieldError.postValue(R.string.field_error_empty)
        if (!houseNotEmpty) houseFieldError.postValue(R.string.field_error_empty)
        if (!areaNotEmpty) areaFieldError.postValue(R.string.field_error_empty)
        if (!yearNotEmpty) yearFieldError.postValue(R.string.field_error_empty)
        if (!roomsNotEmpty) roomsFieldError.postValue(R.string.field_error_empty)
//        if(!descriptionNotEmpty) descriptionFieldError.postValue(R.string.field_error_empty)

        return typeSelected
                && priceNotEmpty
                && districtNotEmpty
                && streetNotEmpty
                && houseNotEmpty
                && areaNotEmpty
                && yearNotEmpty
                && descriptionNotEmpty
    }

    fun onNextBtnClick() {
        _hideKeyboard.postValue(Event(Unit))
        if (checkFields()) {
            if (apartment == null) {
                add.postValue(Unit)
            } else {
                edit.postValue(Unit)
            }
        }
//        _openNext.postValue(
//            Event(
//                Apartment()
//            )
//        )
    }

    /**
     * Network
     */
    private val add = MutableLiveData<Unit>()

    val apartmentResource =
        Transformations.switchMap(add) {
            val map = mutableMapOf<String, Any>()
            map["term"] = type!!.toInt()
            map["price"] = priceFieldText.value!!.toDouble()
            map["district"] = districtFieldText.value ?: ""
            map["street"] = streetFieldText.value ?: ""
            map["house"] = houseFieldText.value ?: ""
            apartmentFieldText.value?.let {
                map["apartment"] = apartmentFieldText.value!!
            }
            map["description"] = descriptionFieldText.value ?: ""
            map["numOfFloor"] = flat
            map["numberOfFloorsOfTheHouse"] = allFloors
            map["numberOfRooms"] = roomsFieldText.value ?: "0"
            map["area"] = areaFieldText.value!!.toInt()
//            "img_link": "https://www.google.com/url?sa=i&url=http%3A%2F%2Fqnimate.com%2Funderstanding-html-img-tag%2F&psig=AOvVaw28-efZt-RPSVt8BRsFXbZC&ust=1588559001237000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCwhcrRlukCFQAAAAAdAAAAABAD",
            map["yearOfConstruction"] = yearFieldText.value!!.toInt()
            map["user"] = user?.id ?: "0"
            map["enabled"] = true
            map["booking_enabled"] = bookingEnabled.value ?: false

            repository.postApartments(map)
        }

    private val edit = MutableLiveData<Unit>()

    val editApartmentResource =
        Transformations.switchMap(edit) {
            val map = mutableMapOf<String, Any>()
            map["term"] = type!!.toInt()
            map["price"] = priceFieldText.value!!.toDouble()
            map["district"] = districtFieldText.value ?: ""
            map["street"] = streetFieldText.value ?: ""
            map["house"] = houseFieldText.value ?: ""
            apartmentFieldText.value?.let {
                map["apartment"] = apartmentFieldText.value!!
            }
            map["description"] = descriptionFieldText.value ?: ""
            map["numOfFloor"] = flat
            map["numberOfFloorsOfTheHouse"] = allFloors
            map["numberOfRooms"] = roomsFieldText.value ?: "0"
            map["area"] = areaFieldText.value!!.toInt()
//            "img_link": "https://www.google.com/url?sa=i&url=http%3A%2F%2Fqnimate.com%2Funderstanding-html-img-tag%2F&psig=AOvVaw28-efZt-RPSVt8BRsFXbZC&ust=1588559001237000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCwhcrRlukCFQAAAAAdAAAAABAD",
            map["yearOfConstruction"] = yearFieldText.value!!.toInt()
            map["user"] = apartment?.user ?: 0
            map["enabled"] = enabledField.value ?: true
            map["booking_enabled"] = bookingEnabled.value ?: false

            repository.editApartment(apartment!!.id!!, map)
        }

    /**
     * Other
     */
    private val _hideKeyboard = MutableLiveData<Event<Unit>>()
    val hideKeyboard: LiveData<Event<Unit>>
        get() = _hideKeyboard

    private val _popBackStack = MutableLiveData<Event<Unit>>()
    val popBackStack: LiveData<Event<Unit>>
        get() = _popBackStack

    fun popBackStack() {
        _popBackStack.postValue(Event(Unit))
    }

}