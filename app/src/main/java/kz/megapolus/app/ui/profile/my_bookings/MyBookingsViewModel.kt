package kz.megapolus.app.ui.profile.my_bookings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.BookingStatusEnums
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.ui_common.callbacks.SwipeToRefreshCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class MyBookingsViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private var user: User? = null

    fun setArgs(user: User?) {
        this.user = user
        _refresh.postValue(Unit)
    }

    private val _refresh = MutableLiveData<Unit>()

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     *
     */
    var isRefreshing = MutableLiveData<Boolean>()

    init {
        isRefreshing.value = false
    }

    val swipeToRefreshCallback = object :
        SwipeToRefreshCallback {
        override fun onSwipeToRefresh() {
            _refresh.value = Unit
            isRefreshing.value = false
        }
    }

    /**
     * Network
     */
    val bookingResource: LiveData<Resource<List<Booking>>> =
        Transformations.switchMap(_refresh) {
            repository.getBookings()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(bookingResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(bookingResource) {
            it.status
        }

    /**
     * Content
     */
    private val _list = MutableLiveData<List<Booking>>()
    val list: LiveData<List<Booking>>
        get() = _list

    val emptyListVisible = MutableLiveData<Boolean>()

    fun onSuccess(list: List<Booking>?) {
        val temp = mutableListOf<Booking>()

        list?.forEach { booking ->
            if (user?.id == booking.user) {
                if (!booking.statuses.isNullOrEmpty()) {
                    booking.statuses.sortedByDescending { it.created }
                    booking.statusText = when (booking.statuses[0].term) {
                        BookingStatusEnums.CONFIRMED.id -> {
                            app.getString(R.string.bookings_detail_status_confirmed)
                        }
                        BookingStatusEnums.WAITING_PAYMENT.id -> {
                            app.getString(R.string.bookings_detail_status_payment)
                        }
                        BookingStatusEnums.PAID.id -> {
                            app.getString(R.string.bookings_detail_status_waiting)
                        }
                        BookingStatusEnums.IN_PROGRESS.id -> {
                            app.getString(R.string.bookings_detail_status_check_in)
                        }
                        BookingStatusEnums.FINISHED.id -> {
                            app.getString(R.string.bookings_detail_status_done)
                        }
                        BookingStatusEnums.CANCELLED.id -> {
                            app.getString(R.string.bookings_detail_status_cancelled)
                        }
                        BookingStatusEnums.REJECTED.id -> {
                            app.getString(R.string.bookings_detail_status_rejected)
                        }
                        else -> {
                            app.getString(R.string.bookings_detail_status_send)
                        }
                    }
                } else {
                    booking.statusText = app.getString(R.string.bookings_detail_status_send)
                }
                temp.add(booking)
            }
            if (user?.id == booking.apartmentOwner) {
                booking.owner = true
                if (!booking.statuses.isNullOrEmpty()) {
                    booking.statuses.sortedBy { it.created }
                    booking.statusText = when (booking.statuses[0].term) {
                        BookingStatusEnums.CONFIRMED.id -> {
                            app.getString(R.string.bookings_detail_status_confirmed)
                        }
                        BookingStatusEnums.WAITING_PAYMENT.id -> {
                            app.getString(R.string.bookings_detail_status_payment)
                        }
                        BookingStatusEnums.PAID.id -> {
                            app.getString(R.string.bookings_detail_status_waiting)
                        }
                        BookingStatusEnums.IN_PROGRESS.id -> {
                            app.getString(R.string.bookings_detail_status_check_in)
                        }
                        BookingStatusEnums.FINISHED.id -> {
                            app.getString(R.string.bookings_detail_status_done)
                        }
                        BookingStatusEnums.CANCELLED.id -> {
                            app.getString(R.string.bookings_detail_status_cancelled)
                        }
                        BookingStatusEnums.REJECTED.id -> {
                            app.getString(R.string.bookings_detail_status_rejected)
                        }
                        else -> {
                            app.getString(R.string.bookings_detail_status_send)
                        }
                    }
                } else {
                    booking.statusText = app.getString(R.string.bookings_detail_status_send)
                }
                temp.add(booking)
            }
        }
        _list.postValue(temp)
        emptyListVisible.postValue(temp.isEmpty())
    }

    /**
     *
     */
    private val _navigate = MutableLiveData<Event<Booking>>()
    val navigate: LiveData<Event<Booking>>
        get() = _navigate

    val recyclerViewItemClickCallback = object :
        RecyclerViewItemClickCallback {
        override fun onRecyclerViewItemClick(any: Any) {
            if (any is Booking) {
                _navigate.postValue(Event(any))
            }
        }
    }

}