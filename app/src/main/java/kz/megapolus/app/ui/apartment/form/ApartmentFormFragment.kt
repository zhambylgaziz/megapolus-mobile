package kz.megapolus.app.ui.apartment.form

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.databinding.FragmentApartmentFormBinding
import kz.megapolus.app.ui.select.SelectDialog
import kz.megapolus.app.ui.user.UserViewModel
import kz.megapolus.app.ui_common.base.BaseFragment
import kz.megapolus.app.utils.KeyboardUtils

class ApartmentFormFragment : BaseFragment() {

    private val args: ApartmentFormFragmentArgs by navArgs()
    private lateinit var binding: FragmentApartmentFormBinding
    private lateinit var viewModel: ApartmentFormViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_apartment_form, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel = getViewModelOfActivity(UserViewModel::class.java)

        viewModel = getViewModel(ApartmentFormViewModel::class.java)
        viewModel.setArgs(userViewModel.getUser(), args.apartment)

        binding.viewModel = viewModel

        initSpinner()

        observeViewModel()
    }

    private fun initSpinner() {
        val adapter: ArrayAdapter<Int> =
            ArrayAdapter<Int>(context!!, android.R.layout.simple_spinner_item, viewModel.numbers)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerFlat.adapter = adapter
        binding.spinnerFlatAll.adapter = adapter
        binding.spinnerFlat.onItemSelectedListener = viewModel.flatSpinnerListener
        binding.spinnerFlatAll.onItemSelectedListener = viewModel.floorsSpinnerListener
    }


    private fun observeViewModel() {
        viewModel.openSelectDialog.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val dialog = SelectDialog.newInstance(it.list)
                    dialog.setTargetFragment(this, it.code.id)
                    dialog.show(fragmentManager!!, "")
                }
            }
        )
        viewModel.apartmentResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        showDialog("Успешно добавлено")
                    }
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.editApartmentResource.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        showDialog("Данные изменены")
                    }
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        handleExceptionDialog(it.exception)
                    }
                }
            }
        )
        viewModel.selectedFlat.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    binding.spinnerFlat.setSelection(it)
                }
            }
        )
        viewModel.selectedAllFlat.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    binding.spinnerFlatAll.setSelection(it)
                }
            }
        )
        viewModel.popBackStack.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    findNavController().popBackStack()
                }
            }
        )
        viewModel.hideKeyboard.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    KeyboardUtils.hideKeyboard(
                        context,
                        binding.btnNext
                    )
                }
            }
        )
    }

    private fun showDialog(message: String) {
        MaterialAlertDialogBuilder(context)
            .setMessage(message)
            .setPositiveButton(context!!.getString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
                viewModel.popBackStack()
            }
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            RequestCodeEnums.SELECT_TYPE.id -> {
                data?.extras?.let {
                    val selectedOption: TitleValue? = it.getParcelable("selectedValue")
                    viewModel.onTypeFieldSelected(selectedOption)
                }
            }
            RequestCodeEnums.SELECT_DISTRICT.id -> {
                data?.extras?.let {
                    val selectedOption: TitleValue? = it.getParcelable("selectedValue")
                    viewModel.onDistrictFieldSelected(selectedOption)
                }
            }
        }
    }

}