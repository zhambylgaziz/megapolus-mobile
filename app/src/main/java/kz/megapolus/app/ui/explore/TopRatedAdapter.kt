package kz.megapolus.app.ui.explore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.house.House
import kz.megapolus.app.databinding.AdapterHouseBinding
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback

class TopRatedAdapter(
    private val recyclerViewItemClickCallback: RecyclerViewItemClickCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<House>() {

        override fun areItemsTheSame(oldItem: House, newItem: House): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: House, newItem: House): Boolean =
            oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list: List<House>) {
        differ.submitList(list)
    }

    companion object {
        private const val VIEW_TYPE_HOUSE = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_HOUSE -> {
                val binding: AdapterHouseBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.adapter_house,
                        parent,
                        false
                    )
                SavedViewHolder(binding)
            }
            else -> {
                throw IllegalStateException("Incorrect ViewType found")
            }
        }
    }

    inner class SavedViewHolder(var binding: AdapterHouseBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initContent(house: House) {
            binding.house = house
            binding.recyclerViewItemClickCallback = recyclerViewItemClickCallback
            binding.ivImage.clipToOutline = true
            binding.executePendingBindings()
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_HOUSE -> {
                val viewHolder = holder as SavedViewHolder
                viewHolder.initContent(differ.currentList[position] as House)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (differ.currentList[position]) {
            is House -> VIEW_TYPE_HOUSE
            else -> throw IllegalStateException("Incorrect ViewType found")
        }

}