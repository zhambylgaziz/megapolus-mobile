package kz.megapolus.app.ui.registration

import android.app.Application
import android.util.Patterns
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.OtpTypeEnums
import kz.megapolus.app.data.enums.app.auth.AuthTypeEnums
import kz.megapolus.app.data.models.auth.Token
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.preferences.Preferences
import kz.megapolus.app.data.repository.auth.AuthRepository
import kz.megapolus.app.ui.registration.password.PasswordRequirementsCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class RegistrationViewModel
@Inject constructor(
    private val app: Application,
    private val repository: AuthRepository,
    private val preferences: Preferences
) : AndroidViewModel(app) {

    /**
     * Email
     */
    val emailFieldText = MutableLiveData<String>()
    val emailFieldError = MutableLiveData<Any>()

    fun onEmailFieldTextChanged() {
        if (!emailFieldText.value.isNullOrEmpty()) {
            emailFieldText.postValue(emailFieldText.value!!.replace(" ", ""))
            emailFieldError.postValue(null)
        }
    }

    private fun emailOk(): Boolean {
        val emailNotEmpty = !emailFieldText.value.isNullOrEmpty()
        val emailValid = isEmailValid()

        return (emailNotEmpty
                && emailValid)
    }

    private fun isEmailValid(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(emailFieldText.value ?: "").matches()
    }

    private fun showEmailErrors() {
        val emailEmpty = !emailFieldText.value.isNullOrEmpty()
        val emailValid = isEmailValid()

        if (!emailEmpty) {
            emailFieldError.postValue(R.string.field_error_empty)
        } else if (!emailValid) {
            emailFieldError.postValue(R.string.field_error_invalid_email)
        }

    }

    /**
     * Login
     */
    val loginFieldText = MutableLiveData<String>()
    val loginFieldError = MutableLiveData<Any>()

    fun onLoginFieldTextChanged() {
        if (!loginFieldText.value.isNullOrEmpty()) {
            loginFieldError.postValue(null)
        }
    }

    /**
     * Password
     */
    private lateinit var passwordRequirementsCallback: PasswordRequirementsCallback

    fun setPasswordRequirementsCallback(passwordRequirementsCallback: PasswordRequirementsCallback) {
        this.passwordRequirementsCallback = passwordRequirementsCallback
    }

    val passwordRequirementsFieldVisible = MutableLiveData<Boolean>()

    val passwordFieldText = MutableLiveData<String>()
    val passwordFieldError = MutableLiveData<Any>()

    fun onPasswordFieldTextChanged() {
        passwordRequirementsFieldVisible.postValue(!checkPasswordRequirements())
        if (!passwordFieldText.value.isNullOrEmpty()) {
            passwordFieldText.postValue(passwordFieldText.value!!.replace(" ", ""))
            passwordFieldError.postValue(null)
        }
    }

    private fun checkPasswordRequirements(): Boolean {
        return passwordRequirementsCallback.passwordOk(
            password = passwordFieldText.value ?: ""
        )
    }

    val listener = View.OnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            passwordRequirementsFieldVisible.postValue(false)
        } else {
            passwordRequirementsFieldVisible.postValue(!checkPasswordRequirements())
        }
    }

    /**
     * LandLord
     */
    val landlordFieldChecked = MutableLiveData<Boolean>()

    /**
     * Registration
     */
    fun onRegisterBtnClick() {
        _hideKeyboard.postValue(Event(Unit))

        val loginNotEmpty = !loginFieldText.value.isNullOrEmpty()
        val passwordNotEmpty = !passwordFieldText.value.isNullOrEmpty()

        val passwordRequirementsOk =
            passwordRequirementsCallback.passwordOk(password = passwordFieldText.value ?: "")

        if (
            emailOk()
            && loginNotEmpty
            && passwordNotEmpty
            && passwordRequirementsOk

        ) {
            registerBtnCLick.postValue(Unit)
        } else {

            if (loginNotEmpty) {
                loginFieldError.postValue(R.string.field_error_empty)
            }
            showEmailErrors()
            if (!passwordNotEmpty) {
                passwordFieldError.postValue(R.string.field_error_empty)
            } else if (!passwordRequirementsOk) {
                passwordFieldError.postValue(R.string.field_password_error)
            }

        }

    }

    /**
     * Network
     */
    private val registerBtnCLick = MutableLiveData<Unit>()

    val registerClientResource =
        Transformations.switchMap(registerBtnCLick) {
            val params = mutableMapOf<String, String>()

            params["username"] = loginFieldText.value!!
            params["email"] = emailFieldText.value!!
            params["password"] = passwordFieldText.value!!

            repository.authRegister(params)
        }

    fun onRegistrationSuccess() {
        signInBtnClick.postValue(Unit)
    }

    private val signInBtnClick = MutableLiveData<Unit>()

    val authResource: LiveData<Resource<Token>> =
        Transformations.switchMap(signInBtnClick) {
            val map = mapOf(
                "username" to loginFieldText.value!!,
                "password" to passwordFieldText.value!!
            )
            repository.authLogin(map)
        }


    private val _updateButton = MutableLiveData<Event<Unit>>()
    val updateButton: LiveData<Event<Unit>>
        get() = _updateButton

    fun onSuccess(token: Token?) {
        token?.authToken?.let {
            preferences.setAppToken(it)
            preferences.setAuthType(AuthTypeEnums.PASSWORD.id)
            _updateButton.postValue(Event(Unit))
        }
    }

    /**
     * Hide keyboard
     */
    private val _hideKeyboard = MutableLiveData<Event<Unit>>()
    val hideKeyboard: LiveData<Event<Unit>>
        get() = _hideKeyboard

}