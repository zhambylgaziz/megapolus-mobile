package kz.megapolus.app.ui.booking

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.DatePatternEnums
import kz.megapolus.app.data.enums.app.RequestCodeEnums
import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.utils.convertDateToString
import kz.megapolus.app.utils.live_data.Event
import java.util.*
import javax.inject.Inject

class BookingViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private var user: User? = null
    private var apartment: Apartment? = null


    fun setArgs(user: User?, apartment: Apartment?) {
        this.user = user
        this.apartment = apartment
    }

    /**
     * Date Start
     */
    var start: Date? = null
    val startFieldText = MutableLiveData<String>()
    val startFieldError = MutableLiveData<String>()

    private val _openSelectDateDialog = MutableLiveData<Event<Int>>()
    val openSelectDateDialog: LiveData<Event<Int>>
        get() = _openSelectDateDialog

    fun onStartFieldClick() {
        _openSelectDateDialog.postValue(Event(RequestCodeEnums.SELECT_FROM_DATE.id))
    }

    fun onDateStartSelected(value: Date?) {
        value?.let {
            this.start = value
            startFieldText.postValue(
                convertDateToString(
                    value,
                    DatePatternEnums.DDMMYYYY_BY_PERIOD.id
                )
            )
            startFieldError.postValue(null)
        }
    }

    /**
     * Date End
     */
    var end: Date? = null
    val endFieldText = MutableLiveData<String>()
    val endFieldError = MutableLiveData<String>()

    fun onEndFieldClick() {
        _openSelectDateDialog.postValue(Event(RequestCodeEnums.SELECT_TO_DATE.id))
    }

    fun onDateEndSelected(value: Date?) {
        value?.let {
            this.end = value
            endFieldText.postValue(
                convertDateToString(
                    value,
                    DatePatternEnums.DDMMYYYY_BY_PERIOD.id
                )
            )
            endFieldError.postValue(null)
        }
    }

    /**
     * Message
     */
    val messageFieldText = MutableLiveData<String>()
    val messageFieldError = MutableLiveData<Any>()

    fun onMessageFieldTextChanged() {
        if (!messageFieldText.value.isNullOrEmpty()) {
            messageFieldError.postValue(null)
        }
    }

    private fun checkFields(): Boolean {
        val startDateSelected = start != null
        val endDateSelected = end != null
        val messageNotEmpty = !messageFieldText.value.isNullOrBlank()

        if (!startDateSelected) startFieldError.postValue(app.getString(R.string.field_error_empty))
        if (!endDateSelected) endFieldError.postValue(app.getString(R.string.field_error_empty))
//        if(!messageNotEmpty) descriptionFieldError.postValue(R.string.field_error_empty)

        return startDateSelected
                && endDateSelected
    }

    fun onNextBtnClick() {
        _hideKeyboard.postValue(Event(Unit))
        if (checkFields()) {
            add.postValue(Unit)
        }
//        _openNext.postValue(
//            Event(
//                Apartment()
//            )
//        )
    }

    /**
     * Network
     */
    private val add = MutableLiveData<Unit>()

    val bookingResource =
        Transformations.switchMap(add) {
            val map = mutableMapOf<String, Any>()
            map["user"] = user?.id ?: "0"
            map["apartment"] = apartment?.id ?: "0"
            start?.let {
                val startDate = convertDateToString(it, DatePatternEnums.YYYYMMDD_BY_DASH.id)
                map["date_start"] = startDate
            }
            end?.let {
                val endDate = convertDateToString(it, DatePatternEnums.YYYYMMDD_BY_DASH.id)
                map["date_end"] = endDate
            }
            map["description"] = messageFieldText.value ?: ""
            map["apartment_owner"] = apartment?.user ?: "0"
            repository.postBooking(map)
        }

    /**
     * Other
     */
    private val _hideKeyboard = MutableLiveData<Event<Unit>>()
    val hideKeyboard: LiveData<Event<Unit>>
        get() = _hideKeyboard

    private val _popBackStack = MutableLiveData<Event<Unit>>()
    val popBackStack: LiveData<Event<Unit>>
        get() = _popBackStack

    fun popBackStack() {
        _popBackStack.postValue(Event(Unit))
    }

}