package kz.megapolus.app.ui.profile.my_bookings.detail.booking_status

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kz.megapolus.app.R
import kz.megapolus.app.data.enums.app.BookingStatusEnums
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.apartment.BookingStatus
import kz.megapolus.app.data.models.auth.User
import kz.megapolus.app.data.models.network.Resource
import kz.megapolus.app.data.models.network.Status
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.ui_common.callbacks.RetryCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class BookingStatusViewModel
@Inject constructor(
    private val app: Application,
    private val repository: ApartmentsRepository
) : AndroidViewModel(app) {

    /**
     * Args
     */
    private lateinit var booking: Booking
    private var user: User? = null

    fun setArgs(booking: Booking, user: User?) {
        this.booking = booking
        this.user = user
        _refresh.postValue(Unit)

        if (user?.id == booking.apartmentOwner) {
            confirmBtnVisible.postValue(true)
            rejectBtnVisible.postValue(true)
            cancelBtnVisible.postValue(false)
            payBtnVisible.postValue(false)
        } else {
            confirmBtnVisible.postValue(false)
            cancelBtnVisible.postValue(true)
            payBtnVisible.postValue(true)
            rejectBtnVisible.postValue(false)
        }

    }

    private val _refresh = MutableLiveData<Unit>()

    val retryCallback = object : RetryCallback {
        override fun onRetryClick() {
            _refresh.value = Unit
        }
    }

    /**
     * Network
     */
    val bookingResource: LiveData<Resource<List<BookingStatus>>> =
        Transformations.switchMap(_refresh) {
            repository.getBookingStatuses()
        }

    val resource: LiveData<Resource<List<Any>>>?
        get() = Transformations.map(bookingResource) {
            it
        }

    val status: LiveData<Status>?
        get() = Transformations.map(bookingResource) {
            it.status
        }

    /**
     * Content
     */
    val statusSend = MutableLiveData<Boolean>()
    val statusSendDateText = MutableLiveData<String>()
    val statusConfirmed = MutableLiveData<Boolean>()
    val statusConfirmedDateText = MutableLiveData<String>()
    val statusPayment = MutableLiveData<Boolean>()
    val statusPaymentDateText = MutableLiveData<String>()
    val statusWaiting = MutableLiveData<Boolean>()
    val statusWaitingDateText = MutableLiveData<String>()
    val statusCheckIn = MutableLiveData<Boolean>()
    val statusCheckInDateText = MutableLiveData<String>()
    val statusDone = MutableLiveData<Boolean>()
    val statusDoneDateText = MutableLiveData<String>()

    val statusDoneFieldText = MutableLiveData<Any>()

    val confirmBtnEnabled = MutableLiveData<Boolean>()
    val cancelBtnEnabled = MutableLiveData<Boolean>()
    val payBtnEnabled = MutableLiveData<Boolean>()
    val rejectBtnEnabled = MutableLiveData<Boolean>()

    val confirmBtnVisible = MutableLiveData<Boolean>()
    val cancelBtnVisible = MutableLiveData<Boolean>()
    val payBtnVisible = MutableLiveData<Boolean>()
    val rejectBtnVisible = MutableLiveData<Boolean>()


    val writeReviewBtnEnabled = MutableLiveData<Boolean>()

    init {
        statusSend.postValue(false)
        statusConfirmed.postValue(false)
        statusPayment.postValue(false)
        statusWaiting.postValue(false)
        statusCheckIn.postValue(false)
        statusDone.postValue(false)

        statusDoneFieldText.postValue(R.string.bookings_detail_status_done)

        confirmBtnEnabled.postValue(false)
        cancelBtnEnabled.postValue(true)
        payBtnEnabled.postValue(false)
        rejectBtnEnabled.postValue(false)

        confirmBtnVisible.postValue(false)
        cancelBtnVisible.postValue(false)
        payBtnVisible.postValue(false)
        rejectBtnVisible.postValue(false)

        writeReviewBtnEnabled.postValue(false)
    }

    fun onSuccess(list: List<BookingStatus>?) {
        if (!list.isNullOrEmpty()) {
            list.sortedBy { it.created }
            list.forEach { status ->
                if (status.booking == booking.id) {
                    when (status.term) {
                        BookingStatusEnums.CREATED.id -> {
                            statusSend.postValue(true)
                            cancelBtnEnabled.postValue(true)
                            confirmBtnEnabled.postValue(true)
                            rejectBtnEnabled.postValue(true)
                            payBtnEnabled.postValue(false)
                            statusSendDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.CONFIRMED.id -> {
                            statusConfirmed.postValue(true)
                            cancelBtnEnabled.postValue(true)
                            confirmBtnEnabled.postValue(false)
                            rejectBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(true)
                            statusConfirmedDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.WAITING_PAYMENT.id -> {
                            statusPayment.postValue(true)
                            cancelBtnEnabled.postValue(true)
                            confirmBtnEnabled.postValue(false)
                            rejectBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(true)
                            statusWaitingDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.PAID.id -> {
                            statusWaiting.postValue(true)
                            cancelBtnEnabled.postValue(false)
                            confirmBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            statusWaitingDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.IN_PROGRESS.id -> {
                            statusCheckIn.postValue(true)
                            cancelBtnEnabled.postValue(false)
                            confirmBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            statusCheckInDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.FINISHED.id -> {
                            statusDone.postValue(true)
                            cancelBtnEnabled.postValue(false)
                            confirmBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            cancelBtnVisible.postValue(false)
                            confirmBtnVisible.postValue(false)
                            rejectBtnVisible.postValue(false)
                            writeReviewBtnEnabled.postValue(true)
                            statusDoneDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.CANCELLED.id -> {
                            statusSend.postValue(false)
                            statusConfirmed.postValue(false)
                            statusPayment.postValue(false)
                            statusWaiting.postValue(false)
                            statusCheckIn.postValue(false)
                            statusDone.postValue(true)
                            cancelBtnEnabled.postValue(false)
                            confirmBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            writeReviewBtnEnabled.postValue(false)
                            cancelBtnVisible.postValue(false)
                            confirmBtnVisible.postValue(false)
                            rejectBtnVisible.postValue(false)
                            statusDoneFieldText.postValue(R.string.bookings_detail_status_cancelled)
                            statusDoneDateText.postValue(status.getCreatedDate())
                        }
                        BookingStatusEnums.REJECTED.id -> {
                            statusSend.postValue(false)
                            statusConfirmed.postValue(false)
                            statusPayment.postValue(false)
                            statusWaiting.postValue(false)
                            statusCheckIn.postValue(false)
                            statusDone.postValue(true)
                            cancelBtnEnabled.postValue(false)
                            confirmBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            payBtnEnabled.postValue(false)
                            writeReviewBtnEnabled.postValue(false)
                            statusDoneFieldText.postValue(R.string.bookings_detail_status_rejected)
                            statusSendDateText.postValue(booking.getCreatedDate())
                            statusDoneDateText.postValue(status.getCreatedDate())
                        }
                        else -> {
                            statusSend.postValue(true)
                            cancelBtnEnabled.postValue(true)
                            confirmBtnEnabled.postValue(true)
                            rejectBtnEnabled.postValue(true)
                            payBtnEnabled.postValue(false)
                        }
                    }
                }
            }
        } else {
            statusSend.postValue(true)
            statusSendDateText.postValue(booking.getCreatedDate())
            cancelBtnEnabled.postValue(true)
            confirmBtnEnabled.postValue(true)
            rejectBtnEnabled.postValue(true)
            payBtnEnabled.postValue(false)
        }
    }

    fun onCancelBtnClick() {
        postStatus.postValue(BookingStatusEnums.CANCELLED)
    }

    fun onRejectBtnClick() {
        postStatus.postValue(BookingStatusEnums.REJECTED)
    }

    fun onConfirmBtnClick() {
        postStatus.postValue(BookingStatusEnums.CONFIRMED)
    }

    fun onPayBtnClick() {

    }

    private val _writeReview = MutableLiveData<Event<Unit>>()
    val writeReview: LiveData<Event<Unit>>
        get() = _writeReview

    fun onWriteReviewClick() {
        _writeReview.postValue(Event(Unit))
    }

    private val postStatus = MutableLiveData<BookingStatusEnums>()

    val statusResource: LiveData<Resource<BookingStatus>> =
        Transformations.switchMap(postStatus) {
            val params = mutableMapOf<String, Any>()

            params["term"] = it.id
            params["booking"] = booking.id ?: 0

            repository.postBookingStatus(params)
        }

    fun onStatusSuccess() {
        _refresh.postValue(Unit)
    }

}