package kz.megapolus.app.ui.profile.my_bookings

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.databinding.AdapterBookingBinding
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback

class MyBookingsAdapter(
    var recyclerViewItemClickCallback: RecyclerViewItemClickCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            return when {
                oldItem is Booking && newItem is Booking -> {
                    oldItem.id == newItem.id
                }
                else -> {
                    false
                }
            }
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            return when {
                oldItem is Booking && newItem is Booking -> {
                    oldItem as Booking == newItem as Booking
                }
                else -> {
                    false
                }
            }
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun getItemCount(): Int = differ.currentList.size

    fun submitList(list: List<Any>) {
        differ.submitList(list)
    }

    companion object {
        private const val VIEW_TYPE_BOOKING = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_BOOKING -> {
                val binding: AdapterBookingBinding =
                    DataBindingUtil.inflate(inflater, R.layout.adapter_booking, parent, false)
                BookingViewHolder(binding)
            }
            else -> {
                throw IllegalStateException("Incorrect ViewType found")
            }
        }
    }

    inner class BookingViewHolder(var binding: AdapterBookingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initContent(booking: Booking) {
            binding.booking = booking
            binding.recyclerViewItemClickCallback = recyclerViewItemClickCallback
            binding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_BOOKING -> {
                val viewHolder = holder as BookingViewHolder
                viewHolder.initContent(differ.currentList[position] as Booking)
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (differ.currentList[position]) {
            is Booking -> VIEW_TYPE_BOOKING
            else -> throw IllegalStateException("Incorrect ViewType found")
        }

}