package kz.megapolus.app.ui.select

import android.app.Application
import android.os.Parcelable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kz.megapolus.app.data.models.custom.TitleValue
import kz.megapolus.app.ui_common.callbacks.RecyclerViewItemClickCallback
import kz.megapolus.app.utils.live_data.Event
import javax.inject.Inject

class SelectViewModel
@Inject constructor(
    private val app: Application
) : AndroidViewModel(app) {

    private val _list = MutableLiveData<List<Any>>()
    val list: LiveData<List<Any>>
        get() = _list

    fun setList(list: List<Any>) {
        val temp = mutableListOf<Any>()

        if (!list.isNullOrEmpty()) {
            temp.addAll(list)
        }

        _list.postValue(temp)
    }

    /**
     *
     */
    private val _setResult = MutableLiveData<Event<Parcelable>>()
    val setResult: LiveData<Event<Parcelable>>
        get() = _setResult

    val onRecyclerViewItemClickListener = object :
        RecyclerViewItemClickCallback {

        override fun onRecyclerViewItemClick(any: Any) {
            when (any) {
                is TitleValue -> {
                    _setResult.postValue(Event(any))
                    _dismiss.postValue(Event(Unit))
                }
            }
        }
    }

    /**
     *
     */
    private val _dismiss = MutableLiveData<Event<Unit>>()
    val dismiss: LiveData<Event<Unit>>
        get() = _dismiss

    fun onCloseBtnClick() {
        _dismiss.postValue(Event(Unit))
    }
}