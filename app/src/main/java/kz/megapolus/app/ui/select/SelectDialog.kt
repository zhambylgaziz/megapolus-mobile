package kz.megapolus.app.ui.select

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kz.megapolus.app.R
import kz.megapolus.app.databinding.DialogSelectBinding
import kz.megapolus.app.ui_common.base.BaseBottomSheetDialogFragment
import kz.megapolus.app.utils.rv.getDividerItemDecoration

class SelectDialog : BaseBottomSheetDialogFragment() {

    private lateinit var list: List<Any>
    private lateinit var binding: DialogSelectBinding
    private lateinit var viewModel: SelectViewModel

    companion object {
        fun newInstance(list: List<Any>): SelectDialog {
            val dialog = SelectDialog()
            dialog.list = list
            return dialog
        }
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.dialog_select, null, false
            )
        binding.lifecycleOwner = this

        dialog.setContentView(binding.root)

        initView()
        setBottomSheetCallback(binding.root)
    }

    private fun initView() {
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        binding.recyclerView.addItemDecoration(
            getDividerItemDecoration(
                context = context!!
            )
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        viewModel = ViewModelProviders.of(
//            this,
//            viewModelFactory
//        ).get(SelectViewModel::class.java)
        viewModel = getViewModelOfActivity(SelectViewModel::class.java)
        viewModel.setList(list)

        binding.viewModel = viewModel
        binding.recyclerView.adapter =
            SelectAdapter(
                viewModel.onRecyclerViewItemClickListener
            )

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.list.observe(
            this,
            Observer {
                (binding.recyclerView.adapter as SelectAdapter).submitList(it)
            })

        viewModel.setResult.observe(
            this,
            Observer {
                it.getContentIfNotHandled()?.let {
                    val bundle = Bundle()
                    bundle.putParcelable("selectedValue", it)

                    val intent = Intent()
                    intent.putExtras(bundle)

                    if (targetFragment != null) {
                        targetFragment!!.onActivityResult(
                            targetRequestCode,
                            Activity.RESULT_OK,
                            intent
                        )
                    }
                }
            }
        )

        viewModel.dismiss.observe(
            this,
            Observer {
                it.getContentIfNotHandled()?.let {
                    dismiss()
                }
            }
        )
    }

}