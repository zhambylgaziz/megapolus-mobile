package kz.megapolus.app.ui_common.callbacks

interface RecyclerViewItemClickCallback {

    fun onRecyclerViewItemClick(any: Any)

}