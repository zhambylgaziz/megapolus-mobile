package kz.megapolus.app.ui_common.callbacks

interface KeyboardKeyClickCallback {

    fun onDigitBtnClick(value: Int)

    fun onDecimalPointBtnClick()

    fun onClearBtnClick()

    fun onClearBtnLongClick(): Boolean

    fun onBiometricsBtnClick()

}