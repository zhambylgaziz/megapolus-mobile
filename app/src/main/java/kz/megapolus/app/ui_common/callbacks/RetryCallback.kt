package kz.megapolus.app.ui_common.callbacks

interface RetryCallback {

    fun onRetryClick()

}
