package kz.megapolus.app.ui_common.callbacks

interface SwipeToRefreshCallback {

    fun onSwipeToRefresh()

}