package kz.megapolus.app.permissions.location_gps;

public class LocationProviderPresenter {

    private LocationProviderView view;

    public LocationProviderPresenter(LocationProviderView view) {
        this.view = view;
    }

    public void onGoogleApiClientConnected() {
        view.requestPermissionToLocation();
    }

    public void onPermissionGrantedToLocation() {
        view.initLastLocationAndStartLocationUpdates();
        view.checkLocationSettings();
    }

    void onPermissionNotGrantedToLocation() {
        //do nothing
    }

}