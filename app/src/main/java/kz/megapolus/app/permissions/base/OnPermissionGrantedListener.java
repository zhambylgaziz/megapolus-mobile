package kz.megapolus.app.permissions.base;

public interface OnPermissionGrantedListener {

    void onPermissionGranted();

    void onPermissionNotGranted();

    void onPermissionExit();

}