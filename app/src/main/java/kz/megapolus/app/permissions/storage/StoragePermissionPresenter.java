package kz.megapolus.app.permissions.storage;


import kz.megapolus.app.permissions.base.PermissionBasePresenter;

public class StoragePermissionPresenter extends PermissionBasePresenter {

    private StoragePermissionView view;

    public StoragePermissionPresenter(StoragePermissionView view) {
        super(view);
        this.view = view;
    }

}