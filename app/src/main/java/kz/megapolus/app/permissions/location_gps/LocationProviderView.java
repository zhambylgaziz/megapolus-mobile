package kz.megapolus.app.permissions.location_gps;

public interface LocationProviderView {

    boolean isPermissionGrantedToLocation();

    void requestPermissionToLocation();

    void initLastLocationAndStartLocationUpdates();

    void checkLocationSettings();

}