package kz.megapolus.app.permissions.location_gps;

import android.location.Location;

public interface LocationProviderListener {

    void onLocationChanged(Location location);

}