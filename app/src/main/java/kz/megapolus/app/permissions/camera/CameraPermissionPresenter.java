package kz.megapolus.app.permissions.camera;


import kz.megapolus.app.permissions.base.PermissionBasePresenter;

public class CameraPermissionPresenter extends PermissionBasePresenter {

    private CameraPermissionView view;

    public CameraPermissionPresenter(CameraPermissionView view) {
        super(view);
        this.view = view;
    }

}