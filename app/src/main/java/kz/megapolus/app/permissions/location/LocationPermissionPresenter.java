package kz.megapolus.app.permissions.location;


import kz.megapolus.app.permissions.base.PermissionBasePresenter;

public class LocationPermissionPresenter extends PermissionBasePresenter {

    private LocationPermissionView view;

    public LocationPermissionPresenter(LocationPermissionView view) {
        super(view);
        this.view = view;
    }

}