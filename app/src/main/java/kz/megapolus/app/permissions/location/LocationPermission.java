package kz.megapolus.app.permissions.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;

import kz.megapolus.app.R;
import kz.megapolus.app.data.enums.app.RequestCodeEnums;
import kz.megapolus.app.permissions.base.OnPermissionGrantedListener;
import kz.megapolus.app.permissions.base.PermissionBase;
import kz.megapolus.app.permissions.enums.PermissionRequestCodeEnums;

public class LocationPermission extends PermissionBase
        implements LocationPermissionView {

    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final String PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;

    public LocationPermission(
            Context context,
            Activity activity,
            Fragment fragment,
            OnPermissionGrantedListener onPermissionGrantedListener,
            boolean isMandatory
    ) {
        super(context, activity, fragment, onPermissionGrantedListener, isMandatory);
    }

    @Override
    protected void initPresenter() {
        presenter = new LocationPermissionPresenter(this);
    }

    @Override
    protected String getManifestPermission() {
        return PERMISSION;
    }

    @Override
    protected String[] getManifestPermissions() {
        return PERMISSIONS;
    }

    @Override
    protected int getRequestCode() {
        return PermissionRequestCodeEnums.LOCATION.getId();
    }

    @Override
    protected int getRequestCodeSettings() {
        return RequestCodeEnums.SETTINGS_LOCATION.getId();
    }

    @Override
    protected String getTitle() {
        return getString(R.string.permission_rationale_title_location);
    }

    @Override
    protected String getMessage() {
        return getString(R.string.permission_rationale_message_location);
    }

    @Override
    protected String getSettingsTitle() {
        return getString(R.string.permission_settings_title_location);
    }

    @Override
    protected String getSettingsMessage() {
        return getString(R.string.permission_settings_message_location);
    }

    @Override
    protected String getPermissionNotGrantedText() {
        return getString(R.string.permission_not_granted_location);
    }

}