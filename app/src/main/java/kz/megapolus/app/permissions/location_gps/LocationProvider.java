package kz.megapolus.app.permissions.location_gps;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import kz.megapolus.app.permissions.base.OnPermissionGrantedListener;
import kz.megapolus.app.permissions.location.LocationPermission;
import kz.megapolus.app.utils.Logger;

public class LocationProvider
        implements LocationProviderView,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    private static final long UPDATE_INTERVAL = 5 * 60000;  /* 60 secs */
    private static final long FASTEST_INTERVAL = 5 * 60000; /* 60 secs */

    private static final int REQUEST_CONNECTION_FAILURE_RESOLUTION_ = 9000;
    private static final int REQUEST_CHECK_SETTINGS = 9001;

    private Context context;
    private Activity activity;
    private Fragment fragment;

    //listeners
    private LocationProviderListener locationProviderListener;
    private OnPermissionGrantedListener onPermissionGrantedListener;

    private LocationProviderPresenter presenter;

    private GoogleApiClient googleApiClient;
    public static Location location;
    private LocationRequest locationRequest;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest locationSettingsRequest;

    /**
     * Permission
     */
    private LocationPermission locationPermission;

    public LocationProvider(
            Context context,
            Activity activity,
            Fragment fragment,
            LocationProviderListener locationProviderListener,
            OnPermissionGrantedListener onPermissionGrantedListener
    ) {
        this.context = context;
        this.activity = activity;
        this.fragment = fragment;

        this.locationProviderListener = locationProviderListener;
        this.onPermissionGrantedListener = onPermissionGrantedListener;

        presenter = new LocationProviderPresenter(this);

        init();
    }

    private void init() {
        initLocationPermission();
        buildGoogleAPIClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    public void requestPermission() {
        buildGoogleAPIClient();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    private void initLocationPermission() {
        locationPermission = new LocationPermission(
                context, activity, fragment, onPermissionGrantedListener, false
        );
    }

    public void onPermissionGranted() {
        presenter.onPermissionGrantedToLocation();
    }

    public void onPermissionNotGranted() {
        presenter.onPermissionNotGrantedToLocation();
    }

    private void buildGoogleAPIClient() {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();
    }

    public void onStart() {
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    public void onStop() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //do nothing
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(activity, REQUEST_CONNECTION_FAILURE_RESOLUTION_);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Logger.e(getClass(), "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        presenter.onGoogleApiClientConnected();
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void initLastLocationAndStartLocationUpdates() {
        if (googleApiClient != null) {
            location = LocationServices.FusedLocationApi.getLastLocation(
                    googleApiClient);

            locationProviderListener.onLocationChanged(location);
            startLocationUpdates();
        }
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        if (!locationPermission.isPermissionGranted()) {
            Toast.makeText(context.getApplicationContext(), "Enable Permissions", Toast.LENGTH_LONG).show();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);

    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        locationProviderListener.onLocationChanged(location);
    }

    public void onDestroy() {
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if (isGoogleApiClientHaveBeenConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean isGoogleApiClientHaveBeenConnected() {
        if (googleApiClient != null
                && googleApiClient.isConnected()) {
            return true;
        }

        return false;
    }

    /**
     * Permission
     */
    @Override
    public boolean isPermissionGrantedToLocation() {
        return locationPermission.isPermissionGranted();
    }

    @Override
    public void requestPermissionToLocation() {
        locationPermission.requestPermission();
    }

    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        locationPermission.onRequestPermissionsResult(
                requestCode, permissions, grantResults
        );
    }

    /**
     *
     */
    @Override
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        googleApiClient,
                        locationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Logger.e(getClass(), "All location settings are satisfied.");

//                Toast.makeText(this, "Location is already on.", Toast.LENGTH_SHORT).show();
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Logger.e(getClass(), "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
//                    Toast.makeText(this, "Location dialog will be open", Toast.LENGTH_SHORT).show();
                    //

                    //move to step 6 in onActivityResult to check what action user has taken on settings dialog
                    status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Logger.e(getClass(), "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Logger.e(getClass(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        locationPermission.onActivityResult(
                requestCode,
                resultCode,
                data
        );
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS: {
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Logger.e(getClass(), "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Logger.e(getClass(), "User chose not to make required location settings changes.");
                        break;
                }
                break;
            }
        }
    }

}