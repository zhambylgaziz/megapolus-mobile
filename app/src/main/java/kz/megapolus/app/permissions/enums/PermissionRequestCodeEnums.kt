package kz.megapolus.app.permissions.enums

enum class PermissionRequestCodeEnums(val id: Int) {

    CAMERA(1),
    STORAGE(2),
    LOCATION(3)

}