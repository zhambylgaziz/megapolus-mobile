package kz.megapolus.app.network.exceptions

import kz.megapolus.app.data.models.network.error.ApiError

class ApiException(val apiError: ApiError) : Exception()