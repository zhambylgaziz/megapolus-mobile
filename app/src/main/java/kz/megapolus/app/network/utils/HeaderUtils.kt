package kz.megapolus.app.network.utils

import android.os.Build
import kz.megapolus.app.BuildConfig
import kz.megapolus.app.data.preferences.Preferences
import javax.inject.Inject

class HeaderUtils
@Inject constructor(
    private val preferences: Preferences,
    private val uniqueID: UniqueID
) {

    fun getAccept(): String = "application/json"

    fun getDeviceId(): String = uniqueID.id()

    fun getAppToken(): String {
        val token = preferences.getAppToken()
        return if (token.isNullOrEmpty()) {
            ""
        } else {
            "Token $token"
        }
    }

    fun getAppVersion(): String = BuildConfig.VERSION_NAME

    fun getLang(): String = preferences.getLanguage() ?: ""

    fun getDeviceBrand(): String = Build.MANUFACTURER

    fun getDeviceModel(): String = Build.MODEL

    fun getDeviceOs(): String = Build.VERSION.RELEASE

    fun getDeviceOsMain(): String = "android"

    fun getDeviceAddInfo(): String = ""

}