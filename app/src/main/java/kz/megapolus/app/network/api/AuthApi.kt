package kz.megapolus.app.network.api

import kz.megapolus.app.data.models.auth.Token
import kz.megapolus.app.data.models.auth.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthApi {

    @POST("auth/v1/token/login")
    suspend fun authLogin(
        @Body params: Map<String, String>
    ): Response<Token>

    @POST("auth/v1/users")
    suspend fun authRegister(
        @Body params: Map<String, String>
    ): Response<User>

    @GET("auth/v1/users/me")
    suspend fun getUser(): Response<User>

    @POST("auth/v1/token/logout")
    suspend fun logout(): Response<Unit>

}