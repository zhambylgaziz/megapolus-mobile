package kz.megapolus.app.network.api

import kz.megapolus.app.data.models.apartment.Apartment
import kz.megapolus.app.data.models.apartment.Booking
import kz.megapolus.app.data.models.apartment.BookingStatus
import kz.megapolus.app.data.models.apartment.Review
import retrofit2.Response
import retrofit2.http.*

interface ApartmentsApi {

    @GET("apartments/")
    suspend fun getApartments(): Response<List<Apartment>>

    @GET("apartments/{id}/")
    suspend fun getApartment(
        @Path("id") id: String
    ): Response<Apartment>

    @POST("apartments/")
    suspend fun postApartment(@Body params: Map<String, @JvmSuppressWildcards Any>): Response<Apartment>

    @PATCH("apartments/{id}/")
    suspend fun patchApartment(
        @Path("id") id: String,
        @Body params: Map<String, @JvmSuppressWildcards Any>
    ): Response<Apartment>

    @DELETE("apartments/{id}/")
    suspend fun deleteApartment(
        @Path("id") id: String
    ): Response<Unit>

    @GET("reviews/")
    suspend fun getReview(): Response<List<Review>>

    @POST("reviews/")
    suspend fun postReview(@Body params: Map<String, @JvmSuppressWildcards Any>): Response<Review>

    @GET("booking/")
    suspend fun getBooking(): Response<List<Booking>>

    @POST("booking/")
    suspend fun postBooking(@Body params: Map<String, @JvmSuppressWildcards Any>): Response<Booking>

    @GET("statuses/")
    suspend fun getBookingStatuses(): Response<List<BookingStatus>>

    @POST("statuses/")
    suspend fun postBookingStatus(@Body params: Map<String, @JvmSuppressWildcards Any>): Response<BookingStatus>

}