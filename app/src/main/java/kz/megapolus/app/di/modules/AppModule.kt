package kz.megapolus.app.di.modules

import dagger.Module
import kz.megapolus.app.di.modules.common.NetworkModule
import kz.megapolus.app.di.modules.common.PreferenceModule
import kz.megapolus.app.di.modules.common.UtilsModule
import kz.megapolus.app.di.view_model.base.ViewModelFactoryModule

@Module(
    includes = [
        NetworkModule::class,
        PreferenceModule::class,
        UtilsModule::class,

        //api
        ApiModule::class,

        //repository
        RepositoryModule::class,

        //
        ViewModelFactoryModule::class,
        ViewModelModule::class
    ]
)
class AppModule {
}