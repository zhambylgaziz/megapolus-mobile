package kz.megapolus.app.di.modules.repository

import dagger.Binds
import dagger.Module
import kz.megapolus.app.data.repository.auth.AuthRepository
import kz.megapolus.app.data.repository.auth.AuthRepositoryImpl
import javax.inject.Singleton

@Module
abstract class AuthRepositoryModule {

    @Binds
    @Singleton
    abstract fun bindAuthRepository(impl: AuthRepositoryImpl): AuthRepository

}