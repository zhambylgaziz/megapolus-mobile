package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.registration.RegistrationFragment

@Module
abstract class RegistrationFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun registrationFragment(): RegistrationFragment

}