package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.apartment.form.ApartmentFormFragment
import kz.megapolus.app.ui.booking.BookingFragment

@Module
abstract class ApartmentsFormFragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun apartmentFormFragment(): ApartmentFormFragment

    @ContributesAndroidInjector
    internal abstract fun bookingFragment(): BookingFragment

}