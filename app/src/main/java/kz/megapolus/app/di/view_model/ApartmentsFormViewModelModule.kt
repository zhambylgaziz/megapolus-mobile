package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.apartment.form.ApartmentFormViewModel
import kz.megapolus.app.ui.booking.BookingViewModel
import kz.megapolus.app.ui.select.SelectViewModel

@Suppress("unused")
@Module
abstract class ApartmentsFormViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ApartmentFormViewModel::class)
    abstract fun bindApartmentFormViewModel(viewModel: ApartmentFormViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectViewModel::class)
    abstract fun bindSelectViewModel(viewModel: SelectViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookingViewModel::class)
    abstract fun bindBookingViewModel(viewModel: BookingViewModel): ViewModel

}