package kz.megapolus.app.di.modules

import dagger.Module
import kz.megapolus.app.di.modules.repository.AboutRepositoryModule
import kz.megapolus.app.di.modules.repository.ApartmentsRepositoryModule
import kz.megapolus.app.di.modules.repository.AuthRepositoryModule

@Module(
    includes = [
        AboutRepositoryModule::class,
        ApartmentsRepositoryModule::class,
        AuthRepositoryModule::class
    ]
)
class RepositoryModule {
}