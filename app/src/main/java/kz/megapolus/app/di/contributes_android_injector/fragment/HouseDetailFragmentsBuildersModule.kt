package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.house_detail.HouseDetailFragment

@Module
abstract class HouseDetailFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun houseDetailFragment(): HouseDetailFragment

}