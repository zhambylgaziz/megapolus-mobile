package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.login.LoginFragment

@Module
abstract class LoginFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun loginFragment(): LoginFragment

}