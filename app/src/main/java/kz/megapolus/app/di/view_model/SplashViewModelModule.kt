package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.onboard.OnboardViewPagerViewModel
import kz.megapolus.app.ui.onboard.other_items.OnboardOtherItemViewModel
import kz.megapolus.app.ui.splash.SplashViewModel

@Suppress("unused")
@Module
abstract class SplashViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnboardViewPagerViewModel::class)
    abstract fun bindOnboardViewPagerViewModel(viewModel: OnboardViewPagerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnboardOtherItemViewModel::class)
    abstract fun bindOnboardOtherItemViewModel(viewModel: OnboardOtherItemViewModel): ViewModel

}