package kz.megapolus.app.di.contributes_android_injector.fragment


import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.explore.ExploreFragment
import kz.megapolus.app.ui.select.SelectDialog

@Module
abstract class ExploreFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun exploreFragment(): ExploreFragment

    @ContributesAndroidInjector
    internal abstract fun selectDialog(): SelectDialog

}