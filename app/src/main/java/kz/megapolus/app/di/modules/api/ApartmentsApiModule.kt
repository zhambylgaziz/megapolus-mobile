package kz.megapolus.app.di.modules.api

import dagger.Module
import dagger.Provides
import kz.megapolus.app.network.api.ApartmentsApi
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApartmentsApiModule {

    @Provides
    @Singleton
    fun provideApartmentsApi(retrofit: Retrofit): ApartmentsApi {
        return retrofit.create(ApartmentsApi::class.java)
    }

}