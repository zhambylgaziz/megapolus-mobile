package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.saved.SavedFragment

@Module
abstract class SavedFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun savedFragment(): SavedFragment

}