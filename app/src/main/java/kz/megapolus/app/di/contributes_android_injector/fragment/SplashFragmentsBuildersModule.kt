package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.onboard.OnboardViewPagerFragment
import kz.megapolus.app.ui.onboard.other_items.OnboardOtherItemFragment
import kz.megapolus.app.ui.splash.SplashFragment

@Module
abstract class SplashFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun splashFragment(): SplashFragment

    @ContributesAndroidInjector
    internal abstract fun onboardViewPagerFragment(): OnboardViewPagerFragment

    @ContributesAndroidInjector
    internal abstract fun onboardOtherItemFragment(): OnboardOtherItemFragment

}