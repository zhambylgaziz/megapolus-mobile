package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.house_detail.HouseDetailViewModel

@Suppress("unused")
@Module
abstract class HouseDetailViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HouseDetailViewModel::class)
    abstract fun bindHouseDetailViewModel(viewModel: HouseDetailViewModel): ViewModel

}