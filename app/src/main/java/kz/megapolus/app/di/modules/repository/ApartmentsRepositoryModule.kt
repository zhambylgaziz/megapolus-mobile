package kz.megapolus.app.di.modules.repository

import dagger.Binds
import dagger.Module
import kz.megapolus.app.data.repository.apartments.ApartmentsRepository
import kz.megapolus.app.data.repository.apartments.ApartmentsRepositoryImpl
import javax.inject.Singleton

@Module
abstract class ApartmentsRepositoryModule {

    @Binds
    @Singleton
    abstract fun bindApartmentsRepository(impl: ApartmentsRepositoryImpl): ApartmentsRepository

}