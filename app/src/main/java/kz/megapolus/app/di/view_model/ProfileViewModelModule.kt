package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.profile.ProfileViewModel
import kz.megapolus.app.ui.profile.about_us.AboutUsViewModel
import kz.megapolus.app.ui.profile.edit.ProfileEditViewModel
import kz.megapolus.app.ui.profile.my_apartments.MyApartmentsViewModel
import kz.megapolus.app.ui.profile.my_bookings.MyBookingsViewModel
import kz.megapolus.app.ui.profile.my_bookings.detail.BookingDetailViewPagerViewModel
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_apartment.BookingApartmentViewModel
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_status.BookingStatusViewModel
import kz.megapolus.app.ui.review.ReviewFormViewModel
import kz.megapolus.app.ui.user.UserViewModel

@Module
abstract class ProfileViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindUserViewModel(viewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileEditViewModel::class)
    abstract fun bindProfileEditViewModel(viewModel: ProfileEditViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyBookingsViewModel::class)
    abstract fun bindMyBookingsViewModel(viewModel: MyBookingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyApartmentsViewModel::class)
    abstract fun bindMyApartmentsViewModel(viewModel: MyApartmentsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookingDetailViewPagerViewModel::class)
    abstract fun bindBookingDetailViewPagerViewModel(viewModel: BookingDetailViewPagerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookingStatusViewModel::class)
    abstract fun bindBookingStatusViewModel(viewModel: BookingStatusViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookingApartmentViewModel::class)
    abstract fun bindBookingApartmentViewModel(viewModel: BookingApartmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutUsViewModel::class)
    abstract fun bindAboutUsViewModel(viewModel: AboutUsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReviewFormViewModel::class)
    abstract fun bindReviewFormViewModel(viewModel: ReviewFormViewModel): ViewModel

}