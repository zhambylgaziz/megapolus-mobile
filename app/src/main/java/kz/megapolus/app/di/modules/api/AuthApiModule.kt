package kz.megapolus.app.di.modules.api

import dagger.Module
import dagger.Provides
import kz.megapolus.app.network.api.AuthApi
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AuthApiModule {

    @Provides
    @Singleton
    fun provideActionsApi(retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }

}