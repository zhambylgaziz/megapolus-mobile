package kz.megapolus.app.di.modules.repository

import dagger.Binds
import dagger.Module
import kz.megapolus.app.data.repository.about.AboutRepository
import kz.megapolus.app.data.repository.about.AboutRepositoryImpl
import javax.inject.Singleton

@Module
abstract class AboutRepositoryModule {

    @Binds
    @Singleton
    abstract fun bindAboutRepository(impl: AboutRepositoryImpl): AboutRepository

}