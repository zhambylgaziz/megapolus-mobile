package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.explore.ExploreViewModel

@Suppress("unused")
@Module
abstract class ExploreViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ExploreViewModel::class)
    abstract fun bindExploreViewModel(viewModel: ExploreViewModel): ViewModel

}