package kz.megapolus.app.di.view_model

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kz.megapolus.app.di.view_model.base.ViewModelKey
import kz.megapolus.app.ui.registration.RegistrationViewModel
import kz.megapolus.app.ui.registration.password.PasswordRequirementsViewModel

@Suppress("unused")
@Module
abstract class RegistrationViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RegistrationViewModel::class)
    abstract fun bindRegistrationViewModel(viewModel: RegistrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PasswordRequirementsViewModel::class)
    abstract fun bindPasswordRequirementsViewModel(viewModel: PasswordRequirementsViewModel): ViewModel

}