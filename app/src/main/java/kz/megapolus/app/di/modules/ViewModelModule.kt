package kz.megapolus.app.di.modules

import dagger.Module
import kz.megapolus.app.di.view_model.*

@Module(
    includes = [
        ActivityViewModelModule::class,
        SplashViewModelModule::class,
        AboutViewModelModule::class,
        ExploreViewModelModule::class,
        SavedViewModelModule::class,
        HouseDetailViewModelModule::class,
        ProfileViewModelModule::class,
        LoginViewModelModule::class,
        RegistrationViewModelModule::class,
        ApartmentsFormViewModelModule::class
    ]
)
class ViewModelModule {
}