package kz.megapolus.app.di.contributes_android_injector.activity

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.di.contributes_android_injector.fragment.*
import kz.megapolus.app.ui.activities.main.MainActivity
import kz.megapolus.app.ui.activities.splash.SplashActivity

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(
        modules = [
            SplashFragmentsBuildersModule::class
        ]
    )
    internal abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector(
        modules = [
            AboutFragmentsBuildersModule::class,
            ExploreFragmentsBuildersModule::class,
            SavedFragmentsBuildersModule::class,
            HouseDetailFragmentsBuildersModule::class,
            ProfileFragmentsBuildersModule::class,
            LoginFragmentsBuildersModule::class,
            RegistrationFragmentsBuildersModule::class,
            ApartmentsFormFragmentBuildersModule::class
        ]
    )
    internal abstract fun mainActivity(): MainActivity

}