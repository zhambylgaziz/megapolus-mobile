package kz.megapolus.app.di.modules

import dagger.Module
import kz.megapolus.app.di.modules.api.AboutApiModule
import kz.megapolus.app.di.modules.api.ApartmentsApiModule
import kz.megapolus.app.di.modules.api.AuthApiModule

@Module(
    includes = [
        AboutApiModule::class,
        ApartmentsApiModule::class,
        AuthApiModule::class
    ]
)
class ApiModule {
}