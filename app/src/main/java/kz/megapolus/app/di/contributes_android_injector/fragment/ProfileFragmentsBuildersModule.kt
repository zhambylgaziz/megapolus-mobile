package kz.megapolus.app.di.contributes_android_injector.fragment

import dagger.Module
import dagger.android.ContributesAndroidInjector
import kz.megapolus.app.ui.profile.ProfileFragment
import kz.megapolus.app.ui.profile.about_us.AboutUsFragment
import kz.megapolus.app.ui.profile.edit.ProfileEditFragment
import kz.megapolus.app.ui.profile.my_apartments.MyApartmentsFragment
import kz.megapolus.app.ui.profile.my_bookings.MyBookingsFragment
import kz.megapolus.app.ui.profile.my_bookings.detail.BookingDetailViewPagerFragment
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_apartment.BookingApartmentFragment
import kz.megapolus.app.ui.profile.my_bookings.detail.booking_status.BookingStatusFragment
import kz.megapolus.app.ui.review.ReviewFormFragment

@Module
abstract class ProfileFragmentsBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun profileFragment(): ProfileFragment

    @ContributesAndroidInjector
    internal abstract fun profileEditFragment(): ProfileEditFragment

    @ContributesAndroidInjector
    internal abstract fun myBookingsFragment(): MyBookingsFragment

    @ContributesAndroidInjector
    internal abstract fun myApartmentsFragment(): MyApartmentsFragment

    @ContributesAndroidInjector
    internal abstract fun bookingDetailViewPagerFragment(): BookingDetailViewPagerFragment

    @ContributesAndroidInjector
    internal abstract fun bookingStatusFragment(): BookingStatusFragment

    @ContributesAndroidInjector
    internal abstract fun bookingApartmentFragment(): BookingApartmentFragment

    @ContributesAndroidInjector
    internal abstract fun aboutUsFragment(): AboutUsFragment

    @ContributesAndroidInjector
    internal abstract fun reviewFormFragment(): ReviewFormFragment

}